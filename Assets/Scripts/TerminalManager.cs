﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

/**
 * Data for Displaying Data in a Terminal-Like Window
 */
public class TerminalManager : MonoBehaviour
{
    public bool allowUserInput = false;
    public VertexAttributeModifier effectModifier;

    [Multiline]
    public string currentUserInput;

    public int currentTerminalDataIndex = 0, previousTerminalDataIndex = -1, endingTerminalDataIndex, originalEndingIndex;

    [Multiline]
    public List<string> terminalDataPool = new List<string>();

    public List<int> userInputCalls = new List<int>();

    public List<UnityEvent> dialogMethodCalls = new List<UnityEvent>();

    public TMP_Text terminalTextData;

    public void OnEnable()
    {
        originalEndingIndex = endingTerminalDataIndex;
    }

    public void Update()
    {
        if (originalEndingIndex == 0 && endingTerminalDataIndex != terminalDataPool.Count)
        {
            // Update Ending Index to Message Pool Count if was not assigned to begin with
            endingTerminalDataIndex = terminalDataPool.Count;
        }

        //effectModifier.enabled = !allowUserInput;

        if (currentTerminalDataIndex == endingTerminalDataIndex && Input.anyKey)
        {
            //
        }

        if (currentTerminalDataIndex != previousTerminalDataIndex && currentTerminalDataIndex < endingTerminalDataIndex)
        {
            if (terminalDataPool.Count >= currentTerminalDataIndex)
            {
                terminalTextData.text += Environment.NewLine + Environment.NewLine + terminalDataPool[currentTerminalDataIndex];
            }

            previousTerminalDataIndex = currentTerminalDataIndex;

            if (userInputCalls.Contains(currentTerminalDataIndex))
            {
                allowUserInput = true;
            }

            if (currentTerminalDataIndex <= dialogMethodCalls.Count && dialogMethodCalls[currentTerminalDataIndex] != null)
            {
                dialogMethodCalls[currentTerminalDataIndex].Invoke();
            }

            if (!allowUserInput)
            {
                currentTerminalDataIndex++;
            }
        }

        if (allowUserInput)
        {
            if (Input.anyKey)
            {
                if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
                {
                    allowUserInput = false;
                    currentUserInput = string.Empty;

                    if (currentTerminalDataIndex < endingTerminalDataIndex)
                    {
                        currentTerminalDataIndex++;
                    }
                }
                else if (Input.GetKeyDown(KeyCode.Backspace) && currentUserInput.Length > 0)
                {
                    currentUserInput = currentUserInput.Remove(currentUserInput.Length - 1, 1);
                    terminalTextData.text = terminalTextData.text.Remove(terminalTextData.text.Length - 1, 1);
                }
                else if (!Input.GetKey(KeyCode.Backspace))
                {
                    currentUserInput += Input.inputString;
                    terminalTextData.text += Input.inputString;
                }
            }
        }
    }
}
