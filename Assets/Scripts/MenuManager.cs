﻿using TMPro;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public static MenuManager Instance;

    public TMP_Text titleText;

    public void Awake()
    {
        Instance = this;
    }

    public void Update()
    {
        // Update Instance as needed
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }

        // Update Title Text as needed
        if (titleText != null && titleText.gameObject.activeInHierarchy && titleText.text != GlobalVariables.Instance.ApplicationTitle)
        {
            titleText.text = GlobalVariables.Instance.ApplicationTitle;
        }
    }
}
