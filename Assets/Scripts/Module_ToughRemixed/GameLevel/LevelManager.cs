﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    public CanvasGroup LevelArea;
    public SongData currentSongData;

    // User Data
    public TMP_Text usernameText, healthText, scoreText;
    // Song Data
    public TMP_Text songNameText, uploaderText;

    public bool inGame, isPaused, songOver;

    // Player Tags: All Tags that can be Identified or are apart of the Player
    // Enemy Tags: All Tags that can be Identified or are apart of an Enemy (Includes Enemy Projectiles)
    // Collidable Enemy Tags: All Tags that the Player can Collide with and can Result in a Damage Event on the player
    // Pusher Tags: Tags that are able to Push Objects with Tags in pushableTags
    // Pushable Tags: Tags that are able to BE pushed by Pusher Tags
    // Insta Kill Tags: Tags that on collission, will result in an Instant Death of anything else
    // Player Projectile Tags: Tags that when collided will result in Damage to EnemyTags
    // Attractable Tags: Tags that can be attracted if allowed
    public List<string> playerTags = new List<string>(), enemyTags = new List<string>(),
        collidableEnemyTags = new List<string>(), pusherTags = new List<string>(), pushableTags = new List<string>(),
        instaKillTags = new List<string>(), playerProjectileTags = new List<string>(), attractableTags = new List<string>();

    public List<PlayerLogic> playerData = new List<PlayerLogic>();

    public List<RootObjectDataLogic> enemyData = new List<RootObjectDataLogic>();

    public void OnEnable()
    {
        Instance = this;
    }

    public void OnDisable()
    {
        // Clear Data
        currentSongData = null;
        inGame = false;
        isPaused = false;
        songOver = false;

        try
        {
            usernameText.text = "UserNameHere";
            healthText.text = "Health: 0/5";
            scoreText.text = "Score: 000000000";

            songNameText.text = "Playing: SongNameHere (AuthorHere)";
            uploaderText.text = "Map By: Player";
        }
        catch (Exception)
        {
            //
        }
    }

    public void Update()
    {
        // Update Instance as needed
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }

        // Go into the Level if not paused and song is not over BUT is not in game and currentSongData is initialized
        if (currentSongData == null && (!inGame && !isPaused && !songOver) && GlobalVariables.Instance.currentSongData != null)
        {
            currentSongData = GlobalVariables.Instance.currentSongData;
            GlobalVariables.Instance.currentSongData = null;

            try
            {
                // Assign Data to text
                usernameText.text = "Clyde";
                healthText.text = "Health: 0/0";
                scoreText.text = "Score: 0 (x1)";

                songNameText.text = "Playing: " + currentSongData.songName + " (" + currentSongData.songAuthor + ")";
                uploaderText.text = "Map By: " + currentSongData.uploader;
            }
            catch (Exception)
            {
                //
            }

            if (!LevelArea.gameObject.activeInHierarchy || LevelArea.alpha < 1)
            {
                GlobalVariables.Instance.FadeUIIn(LevelArea, string.Empty);
            }
            inGame = true;
        }

        if (!AudioManager.Instance.audioSource.isPlaying)
        {
            if (currentSongData != null && AudioManager.Instance.audioSource.clip != currentSongData.songClip)
            {
                AudioManager.Instance.audioSource.clip = currentSongData.songClip;
            }

            if (AudioManager.Instance.audioSource.clip != null)
            {
                AudioManager.Instance.audioSource.Play();
            }
        }
    }
}
