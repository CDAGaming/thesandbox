﻿using Rewired;
using System;
using UnityEngine;

public class PlayerLogic : MonoBehaviour
{
    public RootObjectDataLogic playerLogic;

    [Header("Movement Settings")]
    public Player player;
    public float Horizontal, Vertical, shootTime;
    public bool Fire, Shield;
    public Vector2 movement;

    [Header("Data Settings")]
    public float Score, Combo = 1;
    public string PlayerName;

    public void OnEnable()
    {
        if (LevelManager.Instance != null)
        {
            LevelManager.Instance.playerData.Add(this);

            name = "Player_" + LevelManager.Instance.playerData.Count;
        }

        if (playerLogic.currentMovementType == RootObjectDataLogic.MovementType.Player)
        {
            // Grab the Rewired Player Data if you are controlling this player
            player = ReInput.players.GetPlayer(0);

#if UNITY_STANDALONE || UNITY_EDITOR
            PlayerName = GlobalVariables.Instance.discordManager.currentUser.Username;
#endif
        }
    }

    public void OnDisable()
    {
        ClearPlayerData();
    }

    public void OnDestroy()
    {
        ClearPlayerData();
    }

    public void ClearPlayerData()
    {
        if (LevelManager.Instance != null && LevelManager.Instance.playerData.Contains(this))
        {
            LevelManager.Instance.playerData.Remove(this);
        }
    }

    public void Update()
    {
        // Clamp Score between 0 and the maximum allowed
        Score = Mathf.Max(0, Score);

        if (playerLogic.currentMovementType == RootObjectDataLogic.MovementType.Player)
        {
            #region Control Data
            // Update Control Data if Player is Your Player, and your not dead
            Horizontal = Mathf.Round(player.GetAxisRaw("Horizontal"));
            Vertical = Mathf.Round(player.GetAxisRaw("Vertical"));
            Fire = player.GetButton("Fire");
            Shield = player.GetButton("Shield");

            playerLogic.currentAutoMoveDirection = Horizontal == 0 ? RootObjectDataLogic.AutoMoveDirection.Left : RootObjectDataLogic.AutoMoveDirection.None;

            movement = LevelManager.Instance.isPaused || (playerLogic.currentHealthAmount <= 0 && !playerLogic.invulnerable) ? Vector2.zero : new Vector2(Horizontal * playerLogic.objectSpeed, Vertical * playerLogic.objectSpeed);

            playerLogic.objectBody.velocity = movement;
            #endregion

            // If Player is Yours, Edit the Score, Health, and Username, with yours
            if (LevelManager.Instance != null)
            {
                try
                {
                    LevelManager.Instance.usernameText.text = PlayerName;
                    LevelManager.Instance.scoreText.text = "Score: " + Score + " (x" + Combo + ")";
                    LevelManager.Instance.healthText.text = "Health: " + playerLogic.currentHealthAmount + "/" + playerLogic.maxHealthAmount;
                }
                catch (Exception)
                {
                    // Oh Look, More Possible Log Spam in some areas
                    //Debug.Log("Failed to Assign Text, Continuing...");
                }
            }
        }
    }
}
