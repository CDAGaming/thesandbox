﻿using UnityEngine;

/**
 * Stores Queued Movement Data for Objects
 */
[System.Serializable]
public class ObjectMovement
{
    // Mode to Interpret Target Position
    // Exact: Interpret as an Exact Position to move towards
    // Extend: Add Target Position to Current Position to move in that direction
    public enum PositionMode { Exact, Extend }
    public PositionMode positionMode = PositionMode.Exact;

    // Target Position: Position set in Editor used for Origin Interpretation
    // Current Target: Current Target Position that object aims for, also interprets AutoMoveDirection
    // Current ViewPos Target: Current Target translated to ViewPort Position, used for constraints
    public Vector3 targetPosition, currentTarget, currentViewPosTarget;

    // xCurve: Distance to Travel on the X axis over Time
    // yCurve: Distance to Travel on the Y axis over Time
    // zCurve: Distance to Travel on the Z axis over Time
    // speedCurve: Speed at which Distance is Covered from Current Position to Target
    public AnimationCurve xCurve, yCurve, zCurve, speedCurve;

    // IsCompleted = This Movement has been completed, and can move to the next one if any
    // HasStarted = Movement Data has been properly set up and/or is currently now in progress
    public bool isCompleted = false, hasStarted = false;

    // The Current Time along all MovementCurves
    //
    // ## WARNING ##
    // While Support can be added for them to all be of different times
    // It would be very unwise to attempt to do so, in this case...
    public float currentTime;
}
