﻿using UnityEngine;

public class LittleNandoLogic : MonoBehaviour
{
    public MeshRenderer maskFront, maskBack, hornLeft, hornRight, maskBrow, wireFrame;
    public Color32 frontColor, backColor, sideHornColor, wireColor, browColor;
    public RootObjectDataLogic enemyLogic;

    public bool inChat;
    public Vector3 ForcedPosition;

    public GameObject currentDialogItem;

    public void OnEnable()
    {
        // Stubbed
    }

    public void OnDisable()
    {
        // Stubbed
    }

    public void Update()
    {
        maskFront.material.color = frontColor;
        maskBack.material.color = backColor;
        hornLeft.material.color = sideHornColor;
        hornRight.material.color = sideHornColor;
        maskBrow.material.color = browColor;
        wireFrame.material.color = wireColor;

        // Switch Movement Type if Right Clicked
        enemyLogic.currentMovementType = !Input.GetMouseButton(1) ? RootObjectDataLogic.MovementType.None : RootObjectDataLogic.MovementType.Mouse;
        enemyLogic.currentAutoMoveDirection = Input.GetMouseButton(1) ? RootObjectDataLogic.AutoMoveDirection.None : RootObjectDataLogic.AutoMoveDirection.Left;

        if (enemyLogic.currentMovementType == RootObjectDataLogic.MovementType.Mouse)
        {
            // Rotate Nando if not already rotating and Slash Pushed
            if (Input.GetKeyDown(KeyCode.Slash) && transform.rotation == enemyLogic.originalRotation)
            {
                StartCoroutine(GlobalVariables.Instance.Rotate(transform, enemyLogic.objectSpeed / 4, 360f, Vector3.down));
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                inChat = !inChat;

                if (inChat)
                {
                    // Spawn Dialog Box is entering Chat
                    currentDialogItem = Instantiate(GlobalVariables.Instance.gameDialogObj, transform.parent);

                    currentDialogItem.GetComponent<GameDialogManager>().dialogOwnerPool.Add("Little Nando");
                    currentDialogItem.GetComponent<GameDialogManager>().dialogMessagePool.Add("Hi");

                    currentDialogItem.SetActive(true);
                }
                else
                {
                    Destroy(currentDialogItem);
                    currentDialogItem = null;
                }
            }
        }
    }
}
