using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/**
 * Default Logic Class for Enemies and Players
 */
public class RootObjectDataLogic : MonoBehaviour
{
    public float currentHealthAmount, maxHealthAmount, defaultHealthAmount, objectSpeed, invincibilityCooldown = 2.5f;

    public bool invulnerable, damagableByTheNothing, linearMovement, animatedThisFrame = false, spawnAtRandomY;

    public GameObject lastCollissionObject;

    // Any Vector3 Position containing "View" refers to a Viewpoint Position
    public Vector3 objectPosition, queuedPosition, spawnViewPos, safeViewBoundsMin, safeViewBoundsMax;
    public Vector2 scaledPosition;
    public Quaternion originalRotation;

    // Use Rigid Physics: Only Utilized for If RigidBody Functions should be used (Ex: Force against a colliding object)
    // If Planned to use Collision Logic in this class, this should remain true
    public bool useRigidPhysics = true;
    public Rigidbody objectBody;

    // Ability: This Object is either an Ability or Collectable
    // BodyPart: This Object serves as part of another object
    public enum DataType { Ability, BodyPart, None }
    public DataType currentDataType = DataType.None;
    public string DataID = string.Empty;

    public List<UnityEvent> pickupEvents = new List<UnityEvent>(), hurtEvents = new List<UnityEvent>(),
        deathEvents = new List<UnityEvent>(), initializationEvents = new List<UnityEvent>(),
        updateEvents = new List<UnityEvent>(), collissionEvents = new List<UnityEvent>();

    public enum AttractionMode { Attract, Retract, None };
    public AttractionMode currentAttractionMode = AttractionMode.None;
    public float attractionDistance = 5;

    public enum MovementType { AnimationCurveSingle, AnimationCurveLoop, Player, Mouse, None }
    public MovementType currentMovementType = MovementType.None;

    public enum AutoMoveDirection { Left, Right, None }
    public AutoMoveDirection currentAutoMoveDirection = AutoMoveDirection.None;

    public enum AutoRotateDirection { Clockwise, CounterClockwise, None }
    public AutoRotateDirection currentAutoRotateDirection = AutoRotateDirection.None;

    public enum BoundaryMode { Contain, InstantKill, None }
    public BoundaryMode currentBoundaryMode = BoundaryMode.None;

    public List<ObjectMovement> objectMovements = new List<ObjectMovement>();

    public void OnEnable()
    {
        if (useRigidPhysics)
        {
            // Create RigidBody if Non-Existant)
            if (gameObject.GetComponent<Rigidbody>() == null)
            {
                objectBody = gameObject.AddComponent<Rigidbody>();
            }
        }

        objectBody = gameObject.GetComponent<Rigidbody>();

        // Set Default Spawn Bounds if Empty (Default: Camera Viewport Bounds beteen 0 and 1
        if (safeViewBoundsMin == Vector3.zero)
        {
            safeViewBoundsMin = new Vector3(0, 0, 100);
        }

        if (safeViewBoundsMax == Vector3.zero)
        {
            safeViewBoundsMax = new Vector3(1, 1, 100);
        }

        // Set Origin Position, if any
        if (spawnViewPos == Vector3.zero)
        {
            spawnViewPos = GlobalVariables.Instance.mainCamera.WorldToViewportPoint(transform.position);
        }

        // Set a Random Y Axis Point between 0.0 and 1.0f if wanted
        if (spawnAtRandomY)
        {
            spawnViewPos.y = Random.Range(safeViewBoundsMin.y, safeViewBoundsMax.y);
        }

        transform.position = GlobalVariables.Instance.mainCamera.ViewportToWorldPoint(spawnViewPos);

        currentHealthAmount = defaultHealthAmount;
        maxHealthAmount = defaultHealthAmount;

        originalRotation = transform.rotation;
        objectPosition = transform.position;

        scaledPosition = GlobalVariables.Instance.mainCamera.WorldToViewportPoint(objectPosition);

        // Invoke any Initialization Events for this Object
        foreach (UnityEvent initEvent in initializationEvents)
        {
            initEvent.Invoke();
        }

        if (LevelManager.Instance != null && LevelManager.Instance.enemyTags.Contains(tag))
        {
            LevelManager.Instance.enemyData.Add(this);
        }
    }

    /**
     * Collission Event for Trigger (Non-Pushable) Entities
     */
    public void OnTriggerStay(Collider other)
    {
        PerformCollission(null, other.gameObject);
    }

    /**
     * Collission Event for Non-Trigger (Pushable) Entities
     */
    public void OnCollisionStay(Collision other)
    {
        PerformCollission(other, other.gameObject);
    }

    /**
     * Collission Main Event
     */
    public void PerformCollission(Collision other, GameObject collidingObject)
    {
        // Log Last Collission Object
        lastCollissionObject = collidingObject;

        bool isOtherEnemy = LevelManager.Instance.collidableEnemyTags.Contains(collidingObject.tag);
        bool isEnemy = LevelManager.Instance.collidableEnemyTags.Contains(tag);

        bool isOtherKiller = LevelManager.Instance.instaKillTags.Contains(collidingObject.tag);
        bool isKiller = LevelManager.Instance.instaKillTags.Contains(tag);

        bool isOtherPlayerProjectile = LevelManager.Instance.playerProjectileTags.Contains(collidingObject.tag);

        bool isOtherAttractable = LevelManager.Instance.attractableTags.Contains(collidingObject.tag);
        bool isOtherAbility = collidingObject.GetComponent<RootObjectDataLogic>() != null && collidingObject.GetComponent<RootObjectDataLogic>().currentDataType == DataType.Ability;

        bool isPusherObject = LevelManager.Instance.pusherTags.Contains(tag);
        bool canPushOtherObject = LevelManager.Instance.pushableTags.Contains(collidingObject.tag);

        Debug.Log("Collision between: " + gameObject.name + " and " + collidingObject.name);

        // Execute Any Collission Events First before continuing
        foreach (UnityEvent collissionEvent in collissionEvents)
        {
            collissionEvent.Invoke();
        }

        if (isOtherAttractable && isOtherAbility)
        {
            // Run through any Pickup Events Source Object may have
            foreach (UnityEvent pickupEvent in pickupEvents)
            {
                pickupEvent.Invoke();
            }

            // Then Clear and Destroy the Ability Item
            collidingObject.GetComponent<RootObjectDataLogic>().ClearData(true);
        }
        else if (!invulnerable)
        {
            // Mortal Collision Detection
            //
            // Deduct Health from Object if:
            // - Collission with appropriate projectile if enemy/player
            // - Player Collides with Enemy
            //
            // Destroy/Kill Object if:
            // - Collission from InstaKill Objects (They don't play around anymore ^.^)

            // Collission between objects in push filters
            if (isPusherObject && canPushOtherObject)
            {
                PushObjectFromCollission(other);
            }

            if ((isOtherEnemy && !isEnemy) || (isEnemy && isOtherPlayerProjectile) && (currentHealthAmount > 0 && !invulnerable))
            {
                currentHealthAmount--;

                // Execute Hurt Events when Taking Damage but not Dead
                // This can Include Playing Effects, just like Death Events
                foreach (UnityEvent hurtEvent in hurtEvents)
                {
                    hurtEvent.Invoke();
                }

                StartCoroutine(PlayInvincibilityFrames(invincibilityCooldown));
            }

            if ((isKiller || isOtherKiller) && isKiller != isOtherKiller)
            {
                Destroy((isOtherKiller && !isKiller) ? gameObject : collidingObject);
            }
        }
    }

    /**
     * Pushes a Colliding Object away from the Object collided with
     */
    public void PushObjectFromCollission(Collision other)
    {
        if (useRigidPhysics && other != null)
        {
            // Calculate Angle Between the collision point and the object
            Vector3 dir = other.contacts[0].point - transform.position;
            // We then get the opposite (-Vector3) and normalize it
            dir = -dir.normalized;
            dir.z = transform.position.z;
            // And finally we add force in the direction of dir and multiply it by force. 
            // This will push back the object
            if (objectBody != null)
            {
                objectBody.AddForce(dir);
            }
        }
    }

    public void Update()
    {
        // Clamp Health between 0 and the Maximum Health for this Object
        // We do this to avoid frame-perfect double collissions that occur for some reasons
        currentHealthAmount = Mathf.Clamp(currentHealthAmount, 0.0f, maxHealthAmount);

        if (currentHealthAmount <= 0 && !invulnerable)
        {
            // Clear Object Data if should be dead, resulting in an eventual full destroy
            ClearData(true);
        }
        else
        {
            // Execute any Update Events first, before processing other data
            foreach (UnityEvent updateEvent in updateEvents)
            {
                updateEvent.Invoke();
            }

            if (currentAttractionMode != AttractionMode.None)
            {
                // Attract or Repel Certain Objects within the Attraction Distance
                Collider[] hitColliders = Physics.OverlapSphere(transform.position, attractionDistance);

                foreach (Collider colliderObj in hitColliders)
                {
                    // Do whathever you need here to determine if an object is an attractable object
                    if (LevelManager.Instance.attractableTags.Contains(colliderObj.tag))
                    {
                        Transform attractableObject = colliderObj.transform;
                        attractableObject.position = Vector3.MoveTowards(attractableObject.position, transform.position, ((currentAttractionMode == AttractionMode.Attract) ? 1 : -1) * objectSpeed * Time.deltaTime);
                    }
                }
            }
        }
    }

    public void LateUpdate()
    {
        // Update Positions Object is at, depending on the Movement Type
        animatedThisFrame = false;

        if (currentMovementType == MovementType.Mouse)
        {
            // If MovementType is utilizing the Mouse, Set the Current Position to the MousePosition
            queuedPosition = GlobalVariables.Instance.mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 100));
            transform.position = Vector3.MoveTowards(transform.position, queuedPosition, 0.1f * (linearMovement ? 1 : Vector3.Distance(transform.position, queuedPosition)) * objectSpeed);
        }
        else if ((currentMovementType == MovementType.AnimationCurveSingle || currentMovementType == MovementType.AnimationCurveLoop) && objectMovements != null && objectMovements.Count > 0)
        {
            // Convert and Evaluate Movement Curves towards new Positions
            foreach (ObjectMovement movementObj in objectMovements)
            {
                if (!movementObj.isCompleted)
                {
                    if (!movementObj.hasStarted)
                    {
                        movementObj.currentTarget = (movementObj.positionMode == ObjectMovement.PositionMode.Extend) ? transform.position + movementObj.targetPosition : movementObj.targetPosition;

                        if (currentAutoMoveDirection != AutoMoveDirection.None)
                        {
                            movementObj.currentTarget += new Vector3((currentAutoMoveDirection == AutoMoveDirection.Left ? -1 : 1) * (objectSpeed * Time.deltaTime), 0);
                        }

                        movementObj.currentViewPosTarget = GlobalVariables.Instance.mainCamera.WorldToViewportPoint(movementObj.currentTarget);
                        movementObj.hasStarted = true;

                        // If Containing in Camera Bounds, clamp the Current Target's x and y coordinates
                        // If InstantKill, Kill the Object if it is touching the safe boundaries
                        if (currentBoundaryMode == BoundaryMode.Contain || currentBoundaryMode == BoundaryMode.InstantKill)
                        {
                            movementObj.currentViewPosTarget.x = Mathf.Clamp(movementObj.currentViewPosTarget.x, safeViewBoundsMin.x, safeViewBoundsMax.x);
                            movementObj.currentViewPosTarget.y = Mathf.Clamp(movementObj.currentViewPosTarget.y, safeViewBoundsMin.y, safeViewBoundsMax.y);

                            if (currentBoundaryMode == BoundaryMode.InstantKill && (movementObj.currentViewPosTarget.x == safeViewBoundsMin.x || movementObj.currentViewPosTarget.x == safeViewBoundsMax.x || movementObj.currentViewPosTarget.y == safeViewBoundsMin.y || movementObj.currentViewPosTarget.y == safeViewBoundsMax.y))
                            {
                                Destroy(gameObject);
                            }
                        }
                    }

                    // Transfer Viewport Position back to Current Target Position, to account for constraint changes, if any
                    movementObj.currentTarget = GlobalVariables.Instance.mainCamera.ViewportToWorldPoint(movementObj.currentViewPosTarget);

                    // Current Time in the Curves are Clamped between the first and last keyframe time
                    float maxValue = movementObj.speedCurve != null ? movementObj.speedCurve[movementObj.speedCurve.length - 1].time : 1.0f;

                    movementObj.currentTime += Time.deltaTime;
                    movementObj.currentTime = Mathf.Clamp(movementObj.currentTime, 0.0f, maxValue);

                    // Set Current Target Values based on where the object should move to at a specific keyframe
                    if (movementObj.xCurve != null)
                    {
                        movementObj.currentTarget.x += movementObj.xCurve.Evaluate(movementObj.currentTime);
                    }

                    if (movementObj.yCurve != null)
                    {
                        movementObj.currentTarget.y += movementObj.yCurve.Evaluate(movementObj.currentTime);
                    }

                    if (movementObj.zCurve != null)
                    {
                        movementObj.currentTarget.z += movementObj.zCurve.Evaluate(movementObj.currentTime);
                    }

                    if (transform.position != movementObj.currentTarget && movementObj.currentTime != maxValue)
                    {
                        // If Containing in Camera Bounds, clamp the Current Target's x and y coordinates
                        // If InstantKill, Kill the Object if it is touching the safe boundaries
                        if (currentBoundaryMode == BoundaryMode.Contain || currentBoundaryMode == BoundaryMode.InstantKill)
                        {
                            movementObj.currentViewPosTarget.x = Mathf.Clamp(movementObj.currentViewPosTarget.x, safeViewBoundsMin.x, safeViewBoundsMax.x);
                            movementObj.currentViewPosTarget.y = Mathf.Clamp(movementObj.currentViewPosTarget.y, safeViewBoundsMin.y, safeViewBoundsMax.y);

                            if (currentBoundaryMode == BoundaryMode.InstantKill && (movementObj.currentViewPosTarget.x == safeViewBoundsMin.x || movementObj.currentViewPosTarget.x == safeViewBoundsMax.x || movementObj.currentViewPosTarget.y == safeViewBoundsMin.y || movementObj.currentViewPosTarget.y == safeViewBoundsMax.y))
                            {
                                Destroy(gameObject);
                            }
                        }

                        // Transfer Viewport Position back to Current Target Position, to account for constraint changes, if any
                        movementObj.currentTarget = GlobalVariables.Instance.mainCamera.ViewportToWorldPoint(movementObj.currentViewPosTarget);

                        transform.position = Vector3.MoveTowards(transform.position, movementObj.currentTarget, movementObj.speedCurve != null ? movementObj.speedCurve.Evaluate(movementObj.currentTime) : objectSpeed * Time.deltaTime);

                        animatedThisFrame = true;
                        break;
                    }
                    else
                    {
                        movementObj.isCompleted = true;
                        movementObj.currentTime = 0.0f;
                        movementObj.currentTarget = new Vector3();
                    }
                }
            }

            if (!animatedThisFrame && currentMovementType == MovementType.AnimationCurveLoop)
            {
                foreach (ObjectMovement movementObj in objectMovements)
                {
                    movementObj.isCompleted = false;
                    movementObj.hasStarted = false;
                }
            }
        }
        else
        {
            if (currentAutoMoveDirection != AutoMoveDirection.None)
            {
                transform.position += new Vector3((currentAutoMoveDirection == AutoMoveDirection.Left ? -1 : 1) * (objectSpeed * Time.deltaTime), 0);
            }

            if (transform.rotation == originalRotation)
            {
                if (currentAutoRotateDirection == AutoRotateDirection.Clockwise)
                {
                    StartCoroutine(GlobalVariables.Instance.Rotate(transform, objectSpeed / Time.time, 360f, Vector3.forward));
                }
                else if (currentAutoRotateDirection == AutoRotateDirection.CounterClockwise)
                {
                    StartCoroutine(GlobalVariables.Instance.Rotate(transform, objectSpeed / Time.time, 360f, Vector3.back));
                }
            }
        }

        // If Containing in Camera Bounds, clamp the Current Target's x and y coordinates
        // If InstantKill, Kill the Object if it is touching the safe boundaries
        if (currentBoundaryMode == BoundaryMode.Contain || currentBoundaryMode == BoundaryMode.InstantKill)
        {
            queuedPosition = GlobalVariables.Instance.mainCamera.WorldToViewportPoint(transform.position);

            queuedPosition.x = Mathf.Clamp(queuedPosition.x, safeViewBoundsMin.x, safeViewBoundsMax.x);
            queuedPosition.y = Mathf.Clamp(queuedPosition.y, safeViewBoundsMin.y, safeViewBoundsMax.y);

            if (currentBoundaryMode == BoundaryMode.InstantKill && (queuedPosition.x == safeViewBoundsMin.x || queuedPosition.x == safeViewBoundsMax.x || queuedPosition.y == safeViewBoundsMin.y || queuedPosition.y == safeViewBoundsMax.y))
            {
                Destroy(gameObject);
            }

            transform.position = GlobalVariables.Instance.mainCamera.ViewportToWorldPoint(queuedPosition);
        }

        objectPosition = transform.position;
        scaledPosition = GlobalVariables.Instance.mainCamera.WorldToViewportPoint(objectPosition);
    }

    public void OnDisable()
    {
        ClearData(false);
    }

    public void OnDestroy()
    {
        // This is False as if OnDestroy is being triggered, Unity *should* already be destroying the object
        // If not, Well...uh...hope you dont mind the performance drop...
        ClearData(false);
    }

    /**
     * Yields an Object invulnerable for a specified amount of time
     */
    public IEnumerator PlayInvincibilityFrames(float timeInSeconds)
    {
        if (currentHealthAmount > 0)
        {
            invulnerable = true;
            yield return new WaitForSeconds(timeInSeconds);
            invulnerable = false;
        }
    }

    /**
     * Clears an Object from Level Data and Destroys Object if allowed
     */
    public void ClearData(bool destroyObject)
    {
        // Execute Death Events on Death before anything else
        // This can Include Playing Effects, but shouldn't include Destroying the Game Object
        // Unless, of course, you WANT a bad time, then well...ok.
        foreach (UnityEvent deathEvent in deathEvents)
        {
            deathEvent.Invoke();
        }

        if (LevelManager.Instance.enemyTags.Contains(tag))
        {
            LevelManager.Instance.enemyData.Remove(this);
        }

        if (destroyObject)
        {
            Destroy(gameObject);
        }
    }

    // TOUGH REMIXED -- Event Scripts
    // Temporary Placement for Now

    public void OnAbilityCollection()
    {
        if (lastCollissionObject.GetComponent<RootObjectDataLogic>() != null)
        {
            if (lastCollissionObject.GetComponent<RootObjectDataLogic>().DataID == "Coin" && GetComponent<PlayerLogic>() != null)
            {
                GetComponent<PlayerLogic>().Score += (lastCollissionObject.GetComponent<RootObjectDataLogic>().DataID.Contains("Inverse")) ? -10 : 10;
            }
        }
    }

    // Tough Remixed -- Coin Minigame Script
    // Temporary Placement for now

    bool loopFinished = false;
    int amountOfCoins = 25;
    public void SpawnCoins()
    {
        StartCoroutine(SpawnCoinsPerFrame());
    }

    public IEnumerator SpawnCoinsPerFrame()
    {
        int spawnedCoins;

        while (!loopFinished)
        {
            spawnedCoins = 0;

            while (spawnedCoins != amountOfCoins)
            {
                Instantiate(Resources.Load<GameObject>("Prefabs/Coin"), transform.parent);
                spawnedCoins++;
                yield return new WaitForSeconds(0.5f);
            }

            yield return new WaitForSeconds(2.5f);
        }
    }
}
