﻿using UnityEngine;

/**
 * Root Module Loader
 * Used for Setting an Initial Sub Menu (Or GameObject to enable) to load into upon Enable
 */
public class RootModuleLoader : MonoBehaviour
{
    public static RootModuleLoader Instance;

    public GameObject StartingSubMenu;
    public bool useMenuSystem = true;

    public void OnEnable()
    {
        Instance = this;

        // Set StartingSubMenu to Launch into First
        if (useMenuSystem)
        {
            GlobalVariables.Instance.NewSubMenu = StartingSubMenu.name;
        }
        else
        {
            StartingSubMenu.SetActive(true);
        }
    }

    public void Update()
    {
        // Update Instance as needed
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }
    }

    public void OnDisable()
    {
        GlobalVariables.Instance.NewSubMenu = string.Empty;
    }
}
