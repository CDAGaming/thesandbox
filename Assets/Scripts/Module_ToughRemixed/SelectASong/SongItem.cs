﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

/**
 * Handler for Song Items in Tough Remixed
 * Supported Data: BeatSaber, Doucl, ToughScript Versions 1 and 2
 */
public class SongItem : MonoBehaviour
{
    public string downloadURL, uploader, songName, songAuthor, coverURL, coverLocalPath, downloadPath;
    public string formattedSongName, formattedUploader;
    public TMP_Text songNameData, songUpload;
    public Image songImage;
    public AudioClip songClip;
    public bool imageAssigned = false, songAssigned = false, isDownloaded = false, canPlay = false, currentlyDownloading = false;

    // Specific Map Data - Beat Saber
    public BeatSaber_SongInfo bs_SongInfo;
    public Dictionary<string, BeatSaber_MapData> bs_mapData; // TODO - Stubbed for now

    // Specific Map Data - ToughScript Version 1
    public ToughScript_Version1 ts_MapData;

    public void OnEnable()
    {
        // Formatted Data set to bypass File Errors during download
        formattedSongName = StringHandler.StripFilePattern(songName);
        formattedUploader = StringHandler.StripFilePattern(uploader);

        if (songNameData != null)
        {
            songNameData.text = songName + " - " + songAuthor;
        }
        if (songUpload != null)
        {
            songUpload.text = "Uploaded by: " + uploader;
        }

        // Set Download / Local Song Path if Empty
        // This is true when the song is a local song
        if (downloadPath == string.Empty)
        {
            downloadPath = SelectASong_Loader.Instance.PersistentSongPath + formattedSongName.Trim() + "_" + formattedUploader.Trim() + "/";
        }

        if (!SelectASong_Loader.Instance.songItems.ContainsKey(formattedSongName) ||
            (bs_SongInfo != null && (!SelectASong_Loader.Instance.songItems.ContainsKey(bs_SongInfo._songName) || SelectASong_Loader.Instance.songItems[bs_SongInfo._songName].GetComponent<SongItem>().uploader != uploader)))
        {
            SelectASong_Loader.Instance.songItems.Add(formattedSongName, gameObject);
        }
        else
        {
            Debug.Log("Duplicate Song Item with Name of " + formattedSongName + " detected, destroying duplicate as primary will take over...");
            Destroy(gameObject);
        }
    }

    public void Update()
    {
        if (!imageAssigned)
        {
            imageAssigned = true;

            if (coverURL != string.Empty)
            {
                StartCoroutine(LoadSpriteFromURL(coverURL));
            }
            else if (coverLocalPath != string.Empty)
            {
                songImage.sprite = IMG2Sprite.LoadNewSprite(coverLocalPath);
            }
        }

        if (songNameData != null)
        {
            songNameData.text = (SelectASong_Loader.Instance.currentSongItem == this ? ">> " : "") + songName + " - " + songAuthor;
        }

        if (!songAssigned)
        {
            songAssigned = true;

            string filePath = string.Empty;

            // Specific Parsings for Song File Location
            if (bs_SongInfo != null && bs_SongInfo._songFilename != string.Empty)
            {
                filePath = downloadPath + bs_SongInfo._songFilename;
            }
            else if (ts_MapData != null && ts_MapData.SongLocation != string.Empty)
            {
                filePath = downloadPath + ts_MapData.SongLocation;
            }

            // General Song Data Parsing
            if (filePath != string.Empty)
            {
                if (filePath.EndsWith(".mp3") && !File.Exists(filePath.Replace(".mp3", ".wav")))
                {
                    AudioManager.Instance.ConvertMp3ToWav(filePath, filePath.Replace(".mp3", ".wav"));
                }

                StartCoroutine(AudioManager.Instance.GetAudioClip(songName, new Uri(filePath).AbsoluteUri, filePath.EndsWith(".wav") ? AudioType.WAV : AudioType.OGGVORBIS, false));
            }
        }

        if (AudioManager.Instance.queuedData.ContainsKey(songName) && AudioManager.Instance.queuedData[songName] != null)
        {
            songClip = AudioManager.Instance.queuedData[songName];
            AudioManager.Instance.queuedData.Remove(songName);
        }

        isDownloaded = Directory.Exists(downloadPath);

        canPlay = isDownloaded && songClip != null;
    }

    public void SetSelected()
    {
        SelectASong_Loader.Instance.currentSongItem = this;
    }

    public IEnumerator LoadSpriteFromURL(string urlPath)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(urlPath);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            songImage.sprite = IMG2Sprite.ConvertTextureToSprite(DownloadHandlerTexture.GetContent(www));
        }

        www.Dispose();
    }
}
