﻿using System.Collections.Generic;
using UnityEngine;

/**
 * Conversion Link for Song Item Behavior Scripts
 * TODO: Maybe Remove this later?
 */
public class SongData
{
    public string downloadURL, uploader, songName, songAuthor, coverURL, coverLocalPath, downloadPath;
    public string formattedSongName, formattedUploader;
    public bool imageAssigned = false, isDownloaded = false, currentlyDownloading = false;

    public AudioClip songClip;

    // Specific Map Data - Beat Saber
    public BeatSaber_SongInfo bs_SongInfo;
    public Dictionary<string, BeatSaber_MapData> bs_mapData; // TODO - Stubbed for now

    // Specific Map Data - ToughScript Version 1
    public ToughScript_Version1 ts_MapData;

    public SongData(SongItem originalData)
    {
        downloadURL = originalData.downloadURL;
        uploader = originalData.uploader;
        songName = originalData.songName;
        songAuthor = originalData.songAuthor;
        coverURL = originalData.coverURL;
        coverLocalPath = originalData.coverLocalPath;
        downloadPath = originalData.downloadPath;

        formattedSongName = originalData.formattedSongName;
        formattedUploader = originalData.formattedUploader;

        imageAssigned = originalData.imageAssigned;
        isDownloaded = originalData.isDownloaded;
        currentlyDownloading = originalData.currentlyDownloading;

        songClip = originalData.songClip;

        bs_SongInfo = originalData.bs_SongInfo;
        bs_mapData = originalData.bs_mapData;

        ts_MapData = originalData.ts_MapData;
    }
}
