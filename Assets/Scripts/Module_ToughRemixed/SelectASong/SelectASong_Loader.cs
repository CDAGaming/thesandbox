﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SelectASong_Loader : MonoBehaviour
{
    public static SelectASong_Loader Instance;
    public string PersistentSongPath;

    public int maxSongsPerPage = 10; // HardCoded Limit due to Beat Saver Limitations
    public Dictionary<string, int> queuedLocalSongPool = new Dictionary<string, int>(); // Syntax: directoryPath, expectedPage
    public Dictionary<int, int> localPagePool = new Dictionary<int, int>();
    public int queuedLocalPage = -1; // Start Count One Page before the Default Current Page of 0

    public SongItem currentSongItem;
    public TMP_InputField searchField;
    public TMP_Text loadText, pageText, dropdownValue;
    public GameObject songItem, uninstallItem, loadItem, refreshItem, previousPageItem, nextPageItem;
    public Dictionary<string, GameObject> songItems = new Dictionary<string, GameObject>();
    public BeatSaver_ListData currentBeatSaverData;
    public Transform scrollView;

    public int currentPage, prevPage, nextPage, totalPageCount, totalLocalPageCount;
    public bool retreivingBSData = false, retreivingDouclData = false, retreivingLocalData = false;

    private bool blockFullReset = false;

    public void OnEnable()
    {
        Instance = this;
        PersistentSongPath = GlobalVariables.PersistentDataPath + "/CustomSongs/";
        Refresh();
    }

    public void Update()
    {
        // Update Instance as needed
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }

        // Sync Page Data
        prevPage = currentPage > 0 ? currentPage - 1 : 0;
        nextPage = currentPage < totalPageCount ? currentPage + 1 : totalPageCount;

        // Refresh Button and Text Data
        previousPageItem.GetComponent<Button>().interactable = prevPage < currentPage;
        nextPageItem.GetComponent<Button>().interactable = totalPageCount > currentPage;

        loadItem.SetActive(currentSongItem != null);
        loadItem.GetComponent<Button>().interactable = currentSongItem != null && (currentSongItem.canPlay || !currentSongItem.currentlyDownloading);

        refreshItem.GetComponent<Button>().interactable = !retreivingBSData && !retreivingDouclData && !retreivingLocalData && currentSongItem == null || (currentSongItem != null && !currentSongItem.currentlyDownloading);

        uninstallItem.SetActive(currentSongItem != null && currentSongItem.isDownloaded);

        // TODO: Refactor at a later date (Couldnt get all the Statements to work in one line)
        if (currentSongItem != null && currentSongItem.currentlyDownloading)
        {
            loadText.text = "Downloading...";
        }
        else
        {
            loadText.text = (currentSongItem == null || !currentSongItem.isDownloaded) ? "Download" : "Play";
        }

        // We add One to the Pages here to avoid Issues with 0th Page and vice versa
        pageText.text = "Page " + (currentPage + 1) + " of " + (totalPageCount + 1);

        if (currentSongItem != null && currentSongItem.currentlyDownloading)
        {
            string ID = currentSongItem.formattedSongName.Trim() + "_" + currentSongItem.formattedUploader.Trim();
            currentSongItem.isDownloaded = GlobalVariables.Instance.DownloadPool.ContainsKey(ID) && GlobalVariables.Instance.DownloadPool[ID];

            if (currentSongItem.isDownloaded && currentSongItem.currentlyDownloading)
            {
                // Post-Download Events
                // A Refresh is done to ensure your downloads appear first, and so Local Data can be parsed through
                currentSongItem.currentlyDownloading = false;
                Refresh();
            }
        }
    }

    public void OnDisable()
    {
        ClearObjects();

        // Also Clear Current Page
        currentPage = 0;

        if (GlobalVariables.Instance.NewSubMenu != "GameLevel")
        {
            GlobalVariables.Instance.currentSongData = null;
        }
    }

    public void DownloadOrPlaySelectedSong()
    {
        if (currentSongItem != null)
        {
            if (!currentSongItem.isDownloaded)
            {
                currentSongItem.currentlyDownloading = true;

                // Create Song's Download Directory if it does not exist
                if (!Directory.Exists(currentSongItem.downloadPath))
                {
                    Debug.Log("Creating Song Directory at: " + currentSongItem.downloadPath);
                    Directory.CreateDirectory(currentSongItem.downloadPath);
                    File.SetAttributes(currentSongItem.downloadPath, FileAttributes.Directory);
                    Debug.Log("Created Song Directory at: " + currentSongItem.downloadPath);
                }

                // Download and Extract Data from Song Download
                StartCoroutine(GlobalVariables.Instance.DownloadFile(currentSongItem.formattedSongName.Trim() + "_" + currentSongItem.formattedUploader.Trim(), currentSongItem.downloadURL, currentSongItem.downloadPath + currentSongItem.formattedSongName.Trim() + "_" + currentSongItem.formattedUploader.Trim() + ".zip", false));
            }
            else
            {
                // Play this Map - TODO: To be Implemented soon...
                Debug.Log("Not Implemented yet, Coming Soon...");
                GlobalVariables.Instance.NewSubMenu = "GameLevel";

                GlobalVariables.Instance.currentSongData = new SongData(currentSongItem);
            }
        }
    }

    public void RemoveSong()
    {
        if (currentSongItem != null && Directory.Exists(currentSongItem.downloadPath))
        {
            Debug.Log("Removing Song at " + currentSongItem.downloadPath);
            Directory.Delete(currentSongItem.downloadPath, true);

            // Refresh Songs after Uninstalling to refresh the list properly
            Refresh();
        }
    }

    public void ClearObjects()
    {
        if (songItems != null)
        {
            foreach (GameObject gameObject in songItems.Values)
            {
                Destroy(gameObject);
            }
        }

        songItems.Clear();

        // Also Reset Data
        currentSongItem = null;
        totalPageCount = 0;

        if (!blockFullReset)
        {
            currentPage = 0;
            prevPage = 0;
            nextPage = 0;

            queuedLocalPage = -1;
            queuedLocalSongPool.Clear();
            localPagePool.Clear();
            totalLocalPageCount = 0;
        }
        else
        {
            // Reset Block Flag for next Cycle
            blockFullReset = false;
        }
    }

    public void GoToNextPage()
    {
        currentPage++;
        blockFullReset = true;

        Refresh();
    }

    public void GoToPreviousPage()
    {
        currentPage--;
        blockFullReset = true;

        Refresh();
    }

    public void Refresh()
    {
        // Clear Prior List Data
        ClearObjects();

        // Retreive Local Custom Song Data, before Online Data
        if (Directory.Exists(PersistentSongPath) && Directory.GetDirectories(PersistentSongPath).Length > 0)
        {
            retreivingLocalData = true;

            string[] directories = Directory.GetDirectories(PersistentSongPath);
            Debug.Log("Retreiving Local Song Data from " + directories.Length + " Directories...");

            foreach (string directoryPathOG in directories)
            {
                string directoryPath = directoryPathOG.Replace("\\", "/").Trim();

                // Check if Search Syntax is Ok before Continuing
                if (searchField.text == string.Empty || directoryPath.ToLower().Contains(searchField.text.ToLower()))
                {
                    if (songItems.Count < maxSongsPerPage && ((queuedLocalSongPool.ContainsKey(directoryPath) && queuedLocalSongPool[directoryPath] == currentPage) || queuedLocalSongPool.Count == 0))
                    {
                        string[] splitDirectoryPath = directoryPath.Split("/".ToCharArray());
                        string directoryName = splitDirectoryPath[splitDirectoryPath.Length - 1];

                        Debug.Log("Searching for Song Data in: " + directoryPath);
                        string[] innerFiles = Directory.GetFiles(directoryPath, "*.*", SearchOption.AllDirectories);

                        if (innerFiles.Length > 0)
                        {
                            // Search for Specific Formats based on what is currently supported
                            // Sorry for the sanity lost to those who look at this :(

                            if (File.Exists(directoryPath + "/info.dat"))
                            {
                                // Parse Check for valid Beat Saber Map Data
                                try
                                {
                                    Debug.Log("Attempting to Parse Data as BeatSaber Format...");
                                    BeatSaber_SongInfo songInfo = (BeatSaber_SongInfo)StringHandler.LoadScript(StringHandler.GetClassObj(null, "BeatSaber_SongInfo"), directoryPath + "/info.dat", false);

                                    if (songInfo != null)
                                    {
                                        Debug.Log("BeatSaber Parsing successfull for " + directoryName + " - Adding Data to Pool...");

                                        GameObject tempSongItem = Instantiate(songItem, transform.position, Quaternion.identity, scrollView);

                                        tempSongItem.GetComponent<SongItem>().songName = songInfo._songName;
                                        tempSongItem.GetComponent<SongItem>().songAuthor = songInfo._songAuthorName;
                                        tempSongItem.GetComponent<SongItem>().uploader = songInfo._levelAuthorName;
                                        tempSongItem.GetComponent<SongItem>().coverLocalPath = directoryPath + "/" + songInfo._coverImageFilename;
                                        tempSongItem.GetComponent<SongItem>().bs_SongInfo = songInfo;

                                        tempSongItem.GetComponent<SongItem>().downloadPath = directoryPath + "/";
                                        tempSongItem.GetComponent<SongItem>().isDownloaded = true;

                                        tempSongItem.SetActive(true);
                                    }
                                }
                                catch (Exception)
                                {
                                    Debug.Log("Failed to Parse Data as BeatSaber Format, continuing...");
                                }
                            }
                            else if (File.Exists(directoryPath + "/map.json"))
                            {
                                // Parse Check for valid ToughScript Version 1 Map Data
                                try
                                {
                                    Debug.Log("Attempting to Parse Data as ToughScript v1 Format...");
                                    ToughScript_Version1 songInfo = (ToughScript_Version1)StringHandler.LoadScript(StringHandler.GetClassObj(null, "ToughScript_Version1"), directoryPath + "/map.json", false);

                                    if (songInfo != null)
                                    {
                                        Debug.Log("ToughScript v1 Parsing successfull for " + directoryName + " - Adding Data to Pool...");

                                        GameObject tempSongItem = Instantiate(songItem, transform.position, Quaternion.identity, scrollView);

                                        tempSongItem.GetComponent<SongItem>().songName = songInfo.SongName;
                                        tempSongItem.GetComponent<SongItem>().songAuthor = songInfo.SongArtist;
                                        tempSongItem.GetComponent<SongItem>().uploader = songInfo.MapCreator;
                                        tempSongItem.GetComponent<SongItem>().coverLocalPath = directoryPath + "/" + songInfo.ImageLocation;
                                        tempSongItem.GetComponent<SongItem>().ts_MapData = songInfo;

                                        tempSongItem.GetComponent<SongItem>().downloadPath = directoryPath + "/";
                                        tempSongItem.GetComponent<SongItem>().isDownloaded = true;

                                        tempSongItem.SetActive(true);
                                    }
                                }
                                catch (Exception)
                                {
                                    Debug.Log("Failed to Parse Data as ToughScript v1 Format, continuing...");
                                }
                            }
                            else
                            {
                                Debug.Log("Unable to Parse Data Received from " + directoryPath);
                            }
                        }
                        else
                        {
                            Debug.Log("Skipping and Removing Empty Directory at " + directoryPath);
                            Directory.Delete(directoryPath, true);
                        }
                    }
                    else if (!queuedLocalSongPool.ContainsKey(directoryPath))
                    {
                        // Queue Local Data for Later Retreival
                        if (localPagePool.ContainsKey(queuedLocalPage) && localPagePool[queuedLocalPage] >= maxSongsPerPage)
                        {
                            queuedLocalPage++;
                            totalLocalPageCount++;
                        }

                        if (!localPagePool.ContainsKey(queuedLocalPage))
                        {
                            localPagePool.Add(queuedLocalPage, 0);
                        }

                        queuedLocalSongPool.Add(directoryPath, queuedLocalPage);
                        localPagePool[queuedLocalPage]++;
                    }
                }
            }

            totalPageCount += totalLocalPageCount;
            retreivingLocalData = false;
        }

        // Then Retreive Other Song Items from either Beat Saver or Doucl Services,
        // As well as any other endpoints that may be added later
        // **Don't do this if the Filter is set to only Local Songs
        if (GlobalVariables.Instance.IsConnectedToInternet() && dropdownValue.text.ToLower() != "local")
        {
            if (((ToughRemixed_Config)GlobalVariables.Instance.ConfigDataPool["ToughRemixed"]).BeatSaberURL != string.Empty)
            {
                string url = ((ToughRemixed_Config)GlobalVariables.Instance.ConfigDataPool["ToughRemixed"]).BeatSaberURL + "/api/" + "search/text/" + (currentPage - totalLocalPageCount < 0 ? 0 : currentPage - totalLocalPageCount) + (searchField.text != string.Empty ? ("?q=" + searchField.text.Replace(" ", "%20")) : "");
                //string url = ((ToughRemixed_Config)GlobalVariables.Instance.ConfigDataPool["ToughRemixed"]).BeatSaberURL + "/api/" + (searchField.text != string.Empty ? "search/text/" + (currentPage - totalLocalPageCount < 0 ? 0 : currentPage - totalLocalPageCount) + "?q=" + searchField.text.Replace(" ", "%20") : "maps/" + dropdownValue.text + "/" + (currentPage - totalLocalPageCount < 0 ? 0 : currentPage - totalLocalPageCount));
                Debug.Log(url);
                // Make a UnityWebRequest to Beat Saver's Resources to retrieve songs from them
                StartCoroutine(GetSongsFromBeatSaber(url));
            }

            if (((ToughRemixed_Config)GlobalVariables.Instance.ConfigDataPool["ToughRemixed"]).DouclURL != string.Empty)
            {
                // Make a UnityWebRequest to Doucl's Resources to retreive songs from them
                // Their API is wip at this time so this won't do anything yet
            }
        }
    }

    public IEnumerator GetSongsFromBeatSaber(string endPointURL)
    {
        retreivingBSData = true;

        UnityWebRequest www = UnityWebRequest.Get(endPointURL);

        //www.SetRequestHeader("Content-Type", "application/json");
        www.SetRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            currentBeatSaverData = (BeatSaver_ListData)StringHandler.LoadScript(StringHandler.GetClassObj(null, "BeatSaver_ListData"), www.downloadHandler.text, true);

            if (currentBeatSaverData != null)
            {
                // Set Page Data
                totalPageCount += currentBeatSaverData.lastPage != null ? int.Parse(currentBeatSaverData.lastPage.ToString()) : currentPage + 1;

                if (totalLocalPageCount <= 0 || currentPage > totalLocalPageCount)
                {
                    // If Received Data, Hook Data to SongItems
                    foreach (BeatSaver_ListData.DocsData docsData in currentBeatSaverData.docs)
                    {
                        if (searchField.text.Trim() == string.Empty || docsData.name.Trim().ToLower().Contains(searchField.text.Trim().ToLower()))
                        {
                            GameObject tempSongItem = Instantiate(songItem, transform.position, Quaternion.identity, scrollView);

                            tempSongItem.GetComponent<SongItem>().songName = docsData.metadata.songName;
                            tempSongItem.GetComponent<SongItem>().songAuthor = docsData.metadata.songAuthorName;
                            tempSongItem.GetComponent<SongItem>().uploader = docsData.metadata.levelAuthorName;
                            tempSongItem.GetComponent<SongItem>().downloadURL = docsData.versions[0].downloadURL;
                            tempSongItem.GetComponent<SongItem>().coverURL = docsData.versions[0].coverURL;

                            tempSongItem.SetActive(true);
                        }
                    }
                }
            }
        }
        retreivingBSData = false;
        www.Dispose();
    }
}
