﻿using System.Collections.Generic;

/**
 * JSON Wrapper for BeatSaver List Data
 */
public class BeatSaver_ListData
{
    public List<DocsData> docs { get; set; }

    public int totalDocs { get; set; }
    public object lastPage { get; set; }
    public object prevPage { get; set; }
    public object nextPage { get; set; }

    public class DocsData
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public UploaderData uploader { get; set; }
        public Metadata metadata { get; set; }
        public Stats stats { get; set; }
        public string uploaded { get; set; }
        public bool automapper { get; set; }
        public bool ranked { get; set; }
        public bool qualified { get; set; }
        public List<VersionData> versions { get; set; }
        public string createdAt { get; set; }
        public string updatedAt { get; set; }
        public string lastPublishedAt { get; set; }
        public List<string> tags { get; set; }
        public bool bookmarked { get; set; }
        public string declaredAi { get; set; }
        public bool blRanked { get; set; }
        public bool blQualified { get; set; }
    }

    public class Metadata
    {
        public float bpm { get; set; }
        public int duration { get; set; }
        public string songName { get; set; }
        public string songSubName { get; set; }
        public string songAuthorName { get; set; }
        public string levelAuthorName { get; set; }
    }

    public class Stats
    {
        public int plays { get; set; }
        public int downloads { get; set; }
        public int upvotes { get; set; }
        public int downvotes { get; set; }
        public float score { get; set; }
    }

    public class UploaderData
    {
        public int id { get; set; }
        public string name { get; set; }
        public string hash { get; set; }
        public string avatar { get; set; }
        public string type { get; set; }
        public bool admin { get; set; }
        public bool curator { get; set; }
        public bool seniorCurator { get; set; }
        public bool verifiedMapper { get; set; }
        public string playlistUrl { get; set; }
    }

    public class VersionData
    {
        public string hash { get; set; }
        public string state { get; set; }
        public string createdAt { get; set; }
        public int sageScore { get; set; }
        public List<DifficultyInfo> diffs { get; set; }
        public string downloadURL { get; set; }
        public string coverURL { get; set; }
        public string previewURL { get; set; }
    }

    public class DifficultyInfo
    {
        public float njs { get; set; }
        public float offset { get; set; }
        public int notes { get; set; }
        public int bombs { get; set; }
        public int obstacles { get; set; }
        public float nps { get; set; }
        public float length { get; set; }
        public string characteristic { get; set; }
        public string difficulty { get; set; }
        public int events { get; set; }
        public bool chroma { get; set; }
        public bool me {  get; set; }
        public bool ne { get; set; }
        public bool cinema { get; set; }
        public float seconds { get; set; }
        public ParityData paritySummary { get; set; }
        public int maxScore { get; set; }
    }

    public class ParityData
    {
        public int errors { get; set; }
        public int warns { get; set; }
        public int resets { get; set; }
    }
}
