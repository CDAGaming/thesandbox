﻿using System.Collections.Generic;

/**
 * Main Info Data for Beat Saber Songs
 * From File info.dat primarily
 */
public class BeatSaber_SongInfo
{
    public string _version { get; set; }
    public string _songName { get; set; }
    public string _songSubName { get; set; }
    public string _songAuthorName { get; set; }
    public string _levelAuthorName { get; set; }
    public float _beatsPerMinute { get; set; }
    public float _songTimeOffset { get; set; }
    public float _shuffle { get; set; }
    public float _shufflePeriod { get; set; }
    public float _previewStartTime { get; set; }
    public float _previewDuration { get; set; }
    public string _songFilename { get; set; }
    public string _coverImageFilename { get; set; }
    public string _environmentName { get; set; }
    public CustomData _customData { get; set; }

    public List<DifficultySets> _difficultyBeatmapSets { get; set; }

    public class CustomData
    {
        object[] _contributors { get; set; } // Stubed Function
        public string _customEnvironment { get; set; }
        public string _customEnvironmentHash { get; set; }
    }

    public class DifficultySets
    {
        public string _beatmapCharacteristicName { get; set; }
        public List<BeatMap_DifficultyData> _difficultyBeatmaps { get; set; }
    }

    public class BeatMap_DifficultyData
    {
        public string _difficulty { get; set; }
        public int _difficultyRank { get; set; }
        public string _beatmapFilename { get; set; }
        public float _noteJumpMovementSpeed { get; set; }
        public float _noteJumpStartBeatOffset { get; set; }
        public CustomDifficultyData _customData { get; set; }
    }

    public class CustomDifficultyData
    {
        public string _difficultyLabel { get; set; }
        public float _editorOffset { get; set; }
        public float _editorOldOffset { get; set; }
        public object[] _warnings { get; set; } // Stubbed Function
        public object[] _information { get; set; } // Stubbed Function
        public object[] _suggestions { get; set; } // Stubbed Function
        public object[] _requirements { get; set; } // Stubbed Function
    }
}
