﻿using System.Collections.Generic;

/**
 * Beat Saber Individual Difficulty Map Data
 * This data comes from difficulty data files listed in the SongInfo, such as Normal.dat or Expert.dat
 */
public class BeatSaber_MapData
{
    public string _version { get; set; }
    public List<object> _BPMChanges { get; set; }
    public List<Event> _events { get; set; }
    public List<Note> _notes { get; set; }
    public List<Obstacle> _obstacles { get; set; }
    public List<object> _bookmarks { get; set; }

    public class Event
    {
        public double _time { get; set; }
        public int _type { get; set; }
        public int _value { get; set; }
    }

    public class Note
    {
        public double _time { get; set; }
        public int _lineIndex { get; set; }
        public int _lineLayer { get; set; }
        public int _type { get; set; }
        public int _cutDirection { get; set; }
    }

    public class Obstacle
    {
        public double _time { get; set; }
        public int _lineIndex { get; set; }
        public int _type { get; set; }
        public double _duration { get; set; }
        public int _width { get; set; }
    }
}
