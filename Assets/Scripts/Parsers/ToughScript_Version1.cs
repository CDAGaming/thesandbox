﻿using System.Collections.Generic;

/**
 * Legacy Map Data used for Tough Re-Coded
 * Version: 1.0.0
 * Created by: CDAGaming + TrashCanHero
 */
public class ToughScript_Version1
{
    public int BPM { get; set; }
    public string MapCreator { get; set; }
    public string SongArtist { get; set; }
    public string SongName { get; set; }
    public string SongLocation { get; set; }
    public string ImageLocation { get; set; }
    public List<Event> Events { get; set; }

    public class CycleColorEvent
    {
        public bool Enabled { get; set; }
        public bool RainbowMode { get; set; }
    }

    public class SpawnEvent
    {
        public string EnemyName { get; set; }
        public float Position { get; set; }
    }

    public class Event
    {
        public int Measure { get; set; }
        public int Beat { get; set; }
        public int SubBeat { get; set; }
        public int TSNum { get; set; }
        public int TSDen { get; set; }
        public int MaxSubBeat { get; set; }
        public CycleColorEvent CycleEvent { get; set; }
        public List<SpawnEvent> SpawnEvents { get; set; }
        public bool WhiteFlash { get; set; }
        public bool BlackFlash { get; set; }
        public bool BlackHold { get; set; }
        public bool WhiteHold { get; set; }
        public bool Fast { get; set; }
        public bool Slow { get; set; }
        public bool Zoom { get; set; }
    }


    public static string rawData;
}
