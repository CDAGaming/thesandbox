﻿using System;
using System.Text;
using UnityEngine;

public class DiscordManager : MonoBehaviour
{
#if UNITY_STANDALONE || UNITY_EDITOR
    public string clientID;
    public Discord.ImageHandle currentImageData;
    public Discord.User currentUser;
    public Discord.ActivityManager activityManager;
    public Discord.ApplicationManager applicationManager;
    public Discord.RelationshipManager relationshipManager;
    public Discord.StorageManager storageManager;
    public Discord.StoreManager storeManager;
    public Discord.LobbyTransaction currentLobbyTransaction;
    public Discord.LobbyMemberTransaction currentLobbyMemberTransaction;
    public Discord.ImageManager imageManager;
    public Discord.LobbyManager lobbyManager;
    public Discord.Lobby currentLobby;
    public Discord.UserManager userManager;
    public Discord.Discord discordInstance;

    // RPC Data Tags
    public string state; /* max 128 bytes */
    public string details; /* max 128 bytes */
    public long startTimestamp;
    public long endTimestamp;
    public string largeImageKey; /* max 32 bytes */
    public string largeImageText; /* max 128 bytes */
    public string smallImageKey; /* max 32 bytes */
    public string smallImageText; /* max 128 bytes */
    public string partyId; /* max 128 bytes */
    public int partySize;
    public int partyMax;
    public string matchSecret; /* max 128 bytes */
    public string joinSecret; /* max 128 bytes */
    public string spectateSecret; /* max 128 bytes */
    public bool instance;

    // Request user's avatar data. Sizes can be powers of 2 between 16 and 2048
    public void FetchAvatar(Discord.ImageManager imageManager, long userID)
    {
        imageManager.Fetch(Discord.ImageHandle.User(userID), (result, handle) =>
        {
            {
                if (result == Discord.Result.Ok)
                {
                    // You can use GetTexture2D or GetData within Unity.
                    // Ex: imageManager.GetData(handle)
                    currentImageData = handle;
                    Debug.LogFormat("Image Updated {0} {1}", handle.Id, imageManager.GetData(handle).Length);
                }
                else
                {
                    Debug.LogErrorFormat("Image Error {0}", handle.Id);
                }
            }
        });
    }

    public void UpdateActivity(Discord.Discord discord)
    {
        activityManager = discord.GetActivityManager();

        Discord.Activity activity = new Discord.Activity
        {
            State = state,
            Details = details,
            Timestamps =
            {
                Start = startTimestamp,
                End = endTimestamp,
            },
            Assets =
            {
                LargeImage = largeImageKey,
                LargeText = largeImageText,
                SmallImage = smallImageKey,
                SmallText = smallImageText,
            },
            Instance = instance,
        };

        activityManager.UpdateActivity(activity, result =>
        {
            Debug.LogFormat("Update Activity {0}", result);

            // Send an invite to another user for this activity.
            // Receiver should see an invite in their DM.
            // Use a relationship user's ID for this.
            // activityManager
            //   .SendInvite(
            //       364843917537050624,
            //       Discord.ActivityActionType.Join,
            //       "",
            //       inviteResult =>
            //       {
            //           Debug.LogFormat("Invite {0}", inviteResult);
            //       }
            //   );
        });
    }

    // Update user's activity for your game.
    // Party and secrets are vital.
    // Read https://discordapp.com/developers/docs/rich-presence/how-to for more details.
    public void UpdateActivityWithLobby(Discord.Discord discord, Discord.Lobby lobby)
    {
        activityManager = discord.GetActivityManager();
        lobbyManager = discord.GetLobbyManager();

        Discord.Activity activity = new Discord.Activity
        {
            State = state,
            Details = details,
            Timestamps =
            {
                Start = startTimestamp,
                End = endTimestamp,
            },
            Assets =
            {
                LargeImage = largeImageKey,
                LargeText = largeImageText,
                SmallImage = smallImageKey,
                SmallText = smallImageText,
            },
            Party = {
               Id = lobby.Id.ToString(),
               Size = {
                    CurrentSize = lobbyManager.MemberCount(lobby.Id),
                    MaxSize = (int)lobby.Capacity,
                },
            },
            Secrets = {
                Join = lobbyManager.GetLobbyActivitySecret(lobby.Id),
            },
            Instance = instance,
        };

        activityManager.UpdateActivity(activity, result =>
        {
            Debug.LogFormat("Update Activity {0}", result);

            // Send an invite to another user for this activity.
            // Receiver should see an invite in their DM.
            // Use a relationship user's ID for this.
            // activityManager
            //   .SendInvite(
            //       364843917537050624,
            //       Discord.ActivityActionType.Join,
            //       "",
            //       inviteResult =>
            //       {
            //           Debug.LogFormat("Invite {0}", inviteResult);
            //       }
            //   );
        });
    }

    public void OnEnable()
    {
        if (GlobalVariables.Instance.IsConnectedToInternet())
        {
            // Use the Client ID from the Global Config we have.
            // IF both that, and the Environment Variable are null, use a Preset - TODO

            clientID = Environment.GetEnvironmentVariable("DISCORD_CLIENT_ID");
            if (clientID == null)
            {
                clientID = "597894708949286914";
            }

            discordInstance = new Discord.Discord(long.Parse(clientID), (ulong)Discord.CreateFlags.Default);
            discordInstance.SetLogHook(Discord.LogLevel.Debug, (level, message) =>
            {
                Debug.LogFormat("Log[{0}] {1}", level, message);
            });

            applicationManager = discordInstance.GetApplicationManager();
            // Get the current locale. This can be used to determine what text or audio the user wants.
            Debug.LogFormat("Current Locale: {0}", applicationManager.GetCurrentLocale());
            // Get the current branch. For example alpha or beta.
            Debug.LogFormat("Current Branch: {0}", applicationManager.GetCurrentBranch());
            // If you want to verify information from your game's server then you can
            // grab the access token and send it to your server.
            //
            // This automatically looks for an environment variable passed by the Discord client,
            // if it does not exist the Discord client will focus itself for manual authorization.
            //
            // By-default the SDK grants the identify and rpc scopes.
            // Read more at https://discordapp.com/developers/docs/topics/oauth2
            // applicationManager.GetOAuth2Token((Discord.Result result, ref Discord.OAuth2Token oauth2Token) =>
            // {
            //     Debug.LogFormat("Access Token {0}", oauth2Token.AccessToken);
            // });

            activityManager = discordInstance.GetActivityManager();
            lobbyManager = discordInstance.GetLobbyManager();
            // Received when someone accepts a request to join or invite.
            // Use secrets to receive back the information needed to add the user to the group/party/match
            activityManager.OnActivityJoin += secret =>
            {
                Debug.LogFormat("OnJoin {0}", secret);
                lobbyManager.ConnectLobbyWithActivitySecret(secret, (Discord.Result result, ref Discord.Lobby lobby) =>
                {
                    Debug.LogFormat("Connected to lobby: {0}", lobby.Id);
                    lobbyManager.ConnectNetwork(lobby.Id);
                    lobbyManager.OpenNetworkChannel(lobby.Id, 0, true);
                    foreach (var user in lobbyManager.GetMemberUsers(lobby.Id))
                    {
                        lobbyManager.SendNetworkMessage(lobby.Id, user.Id, 0,
                            Encoding.UTF8.GetBytes(string.Format("Hello, {0}!", user.Username)));
                    }
                    UpdateActivityWithLobby(discordInstance, lobby);
                });
            };
            // Received when someone accepts a request to spectate
            activityManager.OnActivitySpectate += secret =>
            {
                Debug.LogFormat("OnSpectate {0}", secret);
            };
            // A join request has been received. Render the request on the UI.
            activityManager.OnActivityJoinRequest += (ref Discord.User user) =>
            {
                Debug.LogFormat("OnJoinRequest {0} {1}", user.Id, user.Username);
            };
            // An invite has been received. Consider rendering the user / activity on the UI.
            activityManager.OnActivityInvite += (Discord.ActivityActionType Type, ref Discord.User user, ref Discord.Activity activity2) =>
            {
                Debug.LogFormat("OnInvite {0} {1} {2}", Type, user.Username, activity2.Name);
                // activityManager.AcceptInvite(user.Id, result =>
                // {
                //     Debug.LogFormat("AcceptInvite {0}", result);
                // });
            };
            // This is used to register the game in the registry such that Discord can find it.
            // This is only needed by games acquired from other platforms, like Steam.
            // activityManager.RegisterCommand();

            imageManager = discordInstance.GetImageManager();

            userManager = discordInstance.GetUserManager();
            // The auth manager fires events as information about the current user changes.
            // This event will fire once on init.
            //
            // GetCurrentUser will error until this fires once.
            userManager.OnCurrentUserUpdate += () =>
            {
                currentUser = userManager.GetCurrentUser();
                Debug.LogFormat("Retreived Username: " + currentUser.Username);
                Debug.LogFormat("Retreived UserID: " + currentUser.Id);
            };

            relationshipManager = discordInstance.GetRelationshipManager();
            // It is important to assign this handle right away to get the initial relationships refresh.
            // This callback will only be fired when the whole list is initially loaded or was reset
            relationshipManager.OnRefresh += () =>
            {
                // Filter a user's relationship list to be just friends
                relationshipManager.Filter((ref Discord.Relationship relationship) => { return relationship.Type == Discord.RelationshipType.Friend; });
                // Loop over all friends a user has.
                Debug.LogFormat("Relationships Updated: {0}", relationshipManager.Count());
                for (var i = 0; i < Math.Min(relationshipManager.Count(), 10); i++)
                {
                    // Get an individual relationship from the list
                    var r = relationshipManager.GetAt((uint)i);
                    Debug.LogFormat("Relationships: {0} {1} {2} {3}", r.Type, r.User.Username, r.Presence.Status, r.Presence.Activity.Name);

                    // Request relationship's avatar data.
                    FetchAvatar(imageManager, r.User.Id);
                }
            };
            // All following relationship updates are delivered individually.
            // These are fired when a user gets a new friend, removes a friend, or a relationship's presence changes.
            relationshipManager.OnRelationshipUpdate += (ref Discord.Relationship r) =>
            {
                Debug.LogFormat("Relationship Updated: {0} {1} {2} {3}", r.Type, r.User.Username, r.Presence.Status, r.Presence.Activity.Name);
            };

            lobbyManager.OnLobbyMessage += (lobbyID, userID, data) =>
            {
                Debug.LogFormat("Lobby Message: {0} {1}", lobbyID, Encoding.UTF8.GetString(data));
            };
            lobbyManager.OnNetworkMessage += (lobbyId, userId, channelId, data) =>
            {
                Debug.LogFormat("Network Message: {0} {1} {2} {3}", lobbyId, userId, channelId, Encoding.UTF8.GetString(data));
            };
            lobbyManager.OnSpeaking += (lobbyID, userID, speaking) =>
            {
                Debug.LogFormat("Lobby Speaking: {0} {1} {2}", lobbyID, userID, speaking);
            };

            largeImageKey = "missing_tex";
            details = "Loading...";
            startTimestamp = DateTimeOffset.Now.ToUnixTimeSeconds();
            UpdatePresence();
        }
        else
        {
            Debug.Log("Error: Not Connected to Internet, Discord components Disabled");
            gameObject.SetActive(false);
        }
    }

    public void Update()
    {
        try
        {
            // Pump the event look to ensure all callbacks continue to get fired.
            if (discordInstance != null)
            {
                discordInstance.RunCallbacks();
            }

            if (lobbyManager != null)
            {
                lobbyManager.FlushNetwork();
            }
        }
        catch (Exception)
        {
            Debug.Log("Error: Connection Interrupted, Shutting Down!");
            gameObject.SetActive(false);
        }
    }

    /**
     * Forces an RPC Update
     */
    public void UpdatePresence()
    {
        if (discordInstance != null)
        {
            UpdateActivity(discordInstance);
        }
    }

    public void OnDisable()
    {
        if (discordInstance != null)
        {
            discordInstance.Dispose();
        }
    }

#else
 
    void OnEnable()
    {
        // This will remove this component and won't destroy the GameObject
        Destroy(this);
    }
 
#endif
}
