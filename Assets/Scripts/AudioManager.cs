﻿using NAudio.Wave;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioSource audioSource;
    private AudioClip tempClipStorage;

    public Dictionary<string, AudioClip> queuedData = new Dictionary<string, AudioClip>();
    public Dictionary<string, AudioClip> audioPool = new Dictionary<string, AudioClip>();

    public void OnEnable()
    {
        Instance = this;
        audioSource.volume = ((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).MusicVolume;
    }

    public void Update()
    {
        // Update Instance as needed
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }
    }

    /**
     * Use a UnityWebRequest to Retreive either a Local Sound File or one from the Internet
     * ONLY WAV AND VORBIS-TYPE (.egg, .ogg, etc) Codecs are SUPPORTED HERE, for MP3 use the Conversion method below before running this Routine
     */
    public IEnumerator GetAudioClip(string fileName, string fileURL, AudioType audioType, bool storeInPool)
    {
        DownloadHandlerAudioClip downloadHandler = new DownloadHandlerAudioClip(new Uri(fileURL).AbsoluteUri, audioType)
        {
            compressed = false,
            streamAudio = true
        };

        UnityWebRequest request = new UnityWebRequest(new Uri(fileURL).AbsoluteUri, "GET", downloadHandler, null);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            if (audioType == AudioType.WAV || audioType == AudioType.OGGVORBIS)
            {
                tempClipStorage = downloadHandler.audioClip;

                if (tempClipStorage != null)
                {
                    tempClipStorage.name = fileName;
                    queuedData.Add(fileName, tempClipStorage);
                }
            }
            else
            {
                Debug.LogError("Unable to Decrypt or Retreive Audio File due to an Unsupported Extension! (" + fileName + ")");
            }
        }
        request.Dispose();

        if (storeInPool)
        {
            if (!audioPool.ContainsKey(fileName) && !audioPool.ContainsValue(tempClipStorage))
            {
                Debug.Log("Added AudioClip to Audio Pool as: " + fileName);
                audioPool.Add(fileName, tempClipStorage);
                queuedData.Remove(fileName);
            }
        }

        tempClipStorage = null;
    }

    /**
     * Use NAudio to Read a .MP3 file, and Use the PCM Stream Generated
     * To Create a .wav file for use as an AudioClip
     */
    public void ConvertMp3ToWav(string _inPath_, string _outPath_)
    {
        using (Mp3FileReader mp3 = new Mp3FileReader(_inPath_))
        {
            using (WaveStream pcm = WaveFormatConversionStream.CreatePcmStream(mp3))
            {
                WaveFileWriter.CreateWaveFile(_outPath_, pcm);
            }
        }
    }
}
