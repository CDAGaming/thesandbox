﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

/**
 * Manager for Dialog Windows for Modules
 * Not to be Confused with GameDialogManager
 */
public class DialogManager : MonoBehaviour
{
    public string mainTitle;
    public bool enableShadow;

    public Image backGroundObj;
    public TMP_Text windowTitle;
    public GameObject windowBorder, windowTop, windowCloseButton;
    public Shadow dialogShadow;

    public void Awake()
    {
        windowTitle.text = mainTitle;
        dialogShadow.enabled = enableShadow;
    }

    public void CloseDialog()
    {
        Destroy(gameObject);
    }
}
