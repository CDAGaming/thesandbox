﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/**
 * Modifier for TextMeshPro Text Component Verticies
 * Improved Revision modified from Unity for more effects
 * Legacy Sin Wave Script is also available in the Source for normal Text Components
 */
public class VertexAttributeModifier : MonoBehaviour
{
    public enum AnimationMode { VertexColor, Wave, Jitter, Warp, Dangling, Reveal, TeleType };
    public AnimationMode CurrentMeshAnimationMode = AnimationMode.Wave, PreviousMeshAnimationMode = AnimationMode.Wave;
    public AnimationCurve VertexCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.25f, 2.0f), new Keyframe(0.5f, 0), new Keyframe(0.75f, 2.0f), new Keyframe(1, 0f));
    public Dictionary<AnimationMode, IEnumerator> animationEventPool = new Dictionary<AnimationMode, IEnumerator>();

    public float AngleMultiplier = 1.0f;
    public float SpeedMultiplier = 0.025f;
    public float CurveScale = 1.0f;

    private TMP_Text m_TextComponent;
    private bool hasTextChanged;

    private struct VertexAnim
    {
        public float angleRange;
        public float angle;
        public float speed;
    }

    public void OnEnable()
    {
        // Get TextMeshPro Text Component Data
        m_TextComponent = GetComponent<TMP_Text>();

        // Subscribe to event fired when text object has been regenerated.
        TMPro_EventManager.TEXT_CHANGED_EVENT.Add(ON_TEXT_CHANGED);

        // Setup Fresh Data for both the component and the Event Pool
        m_TextComponent.ForceMeshUpdate(); // We force the mesh update in order to get the mesh created so we can have valid data to play with :)   

        animationEventPool.Add(AnimationMode.VertexColor, AnimateVertexColors());
        animationEventPool.Add(AnimationMode.Wave, AnimateVertexPositions());
        animationEventPool.Add(AnimationMode.Jitter, AnimateVertexPositionsII());
        animationEventPool.Add(AnimationMode.Warp, AnimateVertexPositionsIII());
        animationEventPool.Add(AnimationMode.Dangling, AnimateVertexPositionsIV());
        animationEventPool.Add(AnimationMode.Reveal, AnimateVertexPositionsV());
        animationEventPool.Add(AnimationMode.TeleType, AnimateVertexPositionsVI());

        StartCoroutine(animationEventPool[CurrentMeshAnimationMode]);
    }

    public void Update()
    {
        // Update Animation Mode as needed
        if (CurrentMeshAnimationMode != PreviousMeshAnimationMode)
        {
            StopCoroutine(animationEventPool[PreviousMeshAnimationMode]);

            // Reload Fresh/Blank Data
            m_TextComponent.ForceMeshUpdate();

            // Assign New Data for Usage
            PreviousMeshAnimationMode = CurrentMeshAnimationMode;
            StartCoroutine(animationEventPool[CurrentMeshAnimationMode]);
        }
    }

    public void OnDisable()
    {
        // UnSubscribe to event fired when text object has been regenerated.
        TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(ON_TEXT_CHANGED);

        // Reset Text Mesh Data and Force a Clean State
        StopCoroutine(animationEventPool[CurrentMeshAnimationMode]);
        m_TextComponent.ForceMeshUpdate();

        animationEventPool.Clear();
    }

    public void ON_TEXT_CHANGED(Object obj)
    {
        if (obj == m_TextComponent)
            hasTextChanged = true;
    }

    IEnumerator AnimateVertexColors()
    {
        // Force the text object to update right away so we can have geometry to modify right from the start.
        m_TextComponent.ForceMeshUpdate();

        TMP_TextInfo textInfo = m_TextComponent.textInfo;
        int currentCharacter = 0;

        Color32[] newVertexColors;

        while (true)
        {
            int characterCount = textInfo.characterCount;

            // If No Characters then just yield and wait for some text to be added
            if (characterCount == 0)
            {
                yield return new WaitForSeconds(0.25f);
                continue;
            }

            // Get the index of the material used by the current character.
            int materialIndex = textInfo.characterInfo[currentCharacter].materialReferenceIndex;

            // Get the vertex colors of the mesh used by this text element (character or sprite).
            newVertexColors = textInfo.meshInfo[materialIndex].colors32;

            // Get the index of the first vertex used by this text element.
            int vertexIndex = textInfo.characterInfo[currentCharacter].vertexIndex;

            // Only change the vertex color if the text element is visible.
            if (textInfo.characterInfo[currentCharacter].isVisible)
            {
                Color32 c0 = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), 255);

                newVertexColors[vertexIndex + 0] = c0;
                newVertexColors[vertexIndex + 1] = c0;
                newVertexColors[vertexIndex + 2] = c0;
                newVertexColors[vertexIndex + 3] = c0;

                // New function which pushes (all) updated vertex data to the appropriate meshes when using either the Mesh Renderer or CanvasRenderer.
                m_TextComponent.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

                // This last process could be done to only update the vertex data that has changed as opposed to all of the vertex data but it would require extra steps and knowing what type of renderer is used.
                // These extra steps would be a performance optimization but it is unlikely that such optimization will be necessary.
            }

            currentCharacter = (currentCharacter + 1) % characterCount;

            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator AnimateVertexPositions()
    {
        VertexCurve.preWrapMode = WrapMode.Loop;
        VertexCurve.postWrapMode = WrapMode.Loop;

        int loopCount = 0;
        hasTextChanged = true;

        while (true)
        {
            // We force an update of the text object since it would only be updated at the end of the frame. Ie. before this code is executed on the first frame.
            // Alternatively, we could yield and wait until the end of the frame when the text object will be generated.
            m_TextComponent.ForceMeshUpdate();

            TMP_TextInfo textInfo = m_TextComponent.textInfo;
            // Cache the vertex data of the text object as the FX is applied to the original position of the characters.
            TMP_MeshInfo[] cachedMeshInfo = textInfo.CopyMeshInfoVertexData();

            // Get new copy of vertex data if the text has changed.
            if (hasTextChanged)
            {
                // Update the copy of the vertex data for the text object.
                cachedMeshInfo = textInfo.CopyMeshInfoVertexData();

                hasTextChanged = false;
            }

            int characterCount = textInfo.characterCount;

            // If No Characters then just yield and wait for some text to be added
            if (characterCount == 0)
            {
                yield return new WaitForSeconds(SpeedMultiplier);
                continue;
            }

            for (int i = 0; i < characterCount; i++)
            {
                TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

                // Skip characters that are not visible and thus have no geometry to manipulate.
                if (!charInfo.isVisible)
                    continue;

                // Get the index of the material used by the current character.
                int materialIndex = charInfo.materialReferenceIndex;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = charInfo.vertexIndex;

                // Get the cached vertices of the mesh used by this text element (character or sprite).
                Vector3[] newVertexPositions = cachedMeshInfo[materialIndex].vertices;

                float offsetY = VertexCurve.Evaluate((float)i / characterCount + loopCount / 50f) * CurveScale; // Random.Range(-0.25f, 0.25f);                    

                newVertexPositions[vertexIndex + 0].y += offsetY;
                newVertexPositions[vertexIndex + 1].y += offsetY;
                newVertexPositions[vertexIndex + 2].y += offsetY;
                newVertexPositions[vertexIndex + 3].y += offsetY;
            }

            // Push changes into meshes
            for (int i = 0; i < cachedMeshInfo.Length; i++)
            {
                textInfo.meshInfo[i].mesh.vertices = cachedMeshInfo[i].vertices;
                m_TextComponent.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
            }

            loopCount += 1;

            yield return new WaitForSeconds(SpeedMultiplier);
        }
    }

    IEnumerator AnimateVertexPositionsII()
    {
        // We force an update of the text object since it would only be updated at the end of the frame. Ie. before this code is executed on the first frame.
        // Alternatively, we could yield and wait until the end of the frame when the text object will be generated.
        m_TextComponent.ForceMeshUpdate();

        TMP_TextInfo textInfo = m_TextComponent.textInfo;

        Matrix4x4 matrix;

        int loopCount = 0;
        hasTextChanged = true;

        // Create an Array which contains pre-computed Angle Ranges and Speeds for a bunch of characters.
        VertexAnim[] vertexAnim = new VertexAnim[1024];
        for (int i = 0; i < 1024; i++)
        {
            vertexAnim[i].angleRange = Random.Range(10f, 25f);
            vertexAnim[i].speed = Random.Range(1f, 3f);
        }

        // Cache the vertex data of the text object as the Jitter FX is applied to the original position of the characters.
        TMP_MeshInfo[] cachedMeshInfo = textInfo.CopyMeshInfoVertexData();

        while (true)
        {
            // Get new copy of vertex data if the text has changed.
            if (hasTextChanged)
            {
                // Update the copy of the vertex data for the text object.
                cachedMeshInfo = textInfo.CopyMeshInfoVertexData();

                hasTextChanged = false;
            }

            int characterCount = textInfo.characterCount;

            // If No Characters then just yield and wait for some text to be added
            if (characterCount == 0)
            {
                yield return new WaitForSeconds(SpeedMultiplier);
                continue;
            }


            for (int i = 0; i < characterCount; i++)
            {
                TMP_CharacterInfo charInfo = textInfo.characterInfo[i];

                // Skip characters that are not visible and thus have no geometry to manipulate.
                if (!charInfo.isVisible)
                    continue;

                // Retrieve the pre-computed animation data for the given character.
                VertexAnim vertAnim = vertexAnim[i];

                // Get the index of the material used by the current character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                // Get the index of the first vertex used by this text element.
                int vertexIndex = textInfo.characterInfo[i].vertexIndex;

                // Get the cached vertices of the mesh used by this text element (character or sprite).
                Vector3[] sourceVertices = cachedMeshInfo[materialIndex].vertices;

                // Determine the center point of each character at the baseline.
                //Vector2 charMidBasline = new Vector2((sourceVertices[vertexIndex + 0].x + sourceVertices[vertexIndex + 2].x) / 2, charInfo.baseLine);
                // Determine the center point of each character.
                Vector2 charMidBasline = (sourceVertices[vertexIndex + 0] + sourceVertices[vertexIndex + 2]) / 2;

                // Need to translate all 4 vertices of each quad to aligned with middle of character / baseline.
                // This is needed so the matrix TRS is applied at the origin for each character.
                Vector3 offset = charMidBasline;

                Vector3[] destinationVertices = textInfo.meshInfo[materialIndex].vertices;

                destinationVertices[vertexIndex + 0] = sourceVertices[vertexIndex + 0] - offset;
                destinationVertices[vertexIndex + 1] = sourceVertices[vertexIndex + 1] - offset;
                destinationVertices[vertexIndex + 2] = sourceVertices[vertexIndex + 2] - offset;
                destinationVertices[vertexIndex + 3] = sourceVertices[vertexIndex + 3] - offset;

                vertAnim.angle = Mathf.SmoothStep(-vertAnim.angleRange, vertAnim.angleRange, Mathf.PingPong(loopCount / 25f * vertAnim.speed, 1f));
                Vector3 jitterOffset = new Vector3(Random.Range(-.25f, .25f), Random.Range(-.25f, .25f), 0);

                matrix = Matrix4x4.TRS(jitterOffset * CurveScale, Quaternion.Euler(0, 0, Random.Range(-5f, 5f) * AngleMultiplier), Vector3.one);

                destinationVertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 0]);
                destinationVertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 1]);
                destinationVertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 2]);
                destinationVertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(destinationVertices[vertexIndex + 3]);

                destinationVertices[vertexIndex + 0] += offset;
                destinationVertices[vertexIndex + 1] += offset;
                destinationVertices[vertexIndex + 2] += offset;
                destinationVertices[vertexIndex + 3] += offset;

                vertexAnim[i] = vertAnim;
            }

            // Push changes into meshes
            for (int i = 0; i < textInfo.meshInfo.Length; i++)
            {
                textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
                m_TextComponent.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
            }

            loopCount += 1;

            yield return new WaitForSeconds(SpeedMultiplier);
        }
    }

    private AnimationCurve CopyAnimationCurve(AnimationCurve curve)
    {
        AnimationCurve newCurve = new AnimationCurve
        {
            keys = curve.keys
        };

        return newCurve;
    }

    IEnumerator AnimateVertexPositionsIII()
    {
        VertexCurve.preWrapMode = WrapMode.Clamp;
        VertexCurve.postWrapMode = WrapMode.Clamp;

        //Mesh mesh = m_TextComponent.textInfo.meshInfo[0].mesh;

        Vector3[] vertices;
        Matrix4x4 matrix;

        m_TextComponent.havePropertiesChanged = true; // Need to force the TextMeshPro Object to be updated.
        CurveScale *= 10;
        float old_CurveScale = CurveScale;
        AnimationCurve old_curve = CopyAnimationCurve(VertexCurve);

        while (true)
        {
            if (!m_TextComponent.havePropertiesChanged && old_CurveScale == CurveScale && old_curve.keys[1].value == VertexCurve.keys[1].value)
            {
                yield return null;
                continue;
            }

            old_CurveScale = CurveScale;
            old_curve = CopyAnimationCurve(VertexCurve);

            m_TextComponent.ForceMeshUpdate(); // Generate the mesh and populate the textInfo with data we can use and manipulate.

            TMP_TextInfo textInfo = m_TextComponent.textInfo;
            int characterCount = textInfo.characterCount;


            if (characterCount == 0) continue;

            //vertices = textInfo.meshInfo[0].vertices;
            //int lastVertexIndex = textInfo.characterInfo[characterCount - 1].vertexIndex;

            float boundsMinX = m_TextComponent.bounds.min.x;  //textInfo.meshInfo[0].mesh.bounds.min.x;
            float boundsMaxX = m_TextComponent.bounds.max.x;  //textInfo.meshInfo[0].mesh.bounds.max.x;



            for (int i = 0; i < characterCount; i++)
            {
                if (!textInfo.characterInfo[i].isVisible)
                    continue;

                int vertexIndex = textInfo.characterInfo[i].vertexIndex;

                // Get the index of the mesh used by this character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                vertices = textInfo.meshInfo[materialIndex].vertices;

                // Compute the baseline mid point for each character
                Vector3 offsetToMidBaseline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, textInfo.characterInfo[i].baseLine);
                //float offsetY = VertexCurve.Evaluate((float)i / characterCount + loopCount / 50f); // Random.Range(-0.25f, 0.25f);

                // Apply offset to adjust our pivot point.
                vertices[vertexIndex + 0] += -offsetToMidBaseline;
                vertices[vertexIndex + 1] += -offsetToMidBaseline;
                vertices[vertexIndex + 2] += -offsetToMidBaseline;
                vertices[vertexIndex + 3] += -offsetToMidBaseline;

                // Compute the angle of rotation for each character based on the animation curve
                float x0 = (offsetToMidBaseline.x - boundsMinX) / (boundsMaxX - boundsMinX); // Character's position relative to the bounds of the mesh.
                float x1 = x0 + 0.0001f;
                float y0 = VertexCurve.Evaluate(x0) * CurveScale;
                float y1 = VertexCurve.Evaluate(x1) * CurveScale;

                Vector3 horizontal = new Vector3(1, 0, 0);
                //Vector3 normal = new Vector3(-(y1 - y0), (x1 * (boundsMaxX - boundsMinX) + boundsMinX) - offsetToMidBaseline.x, 0);
                Vector3 tangent = new Vector3(x1 * (boundsMaxX - boundsMinX) + boundsMinX, y1) - new Vector3(offsetToMidBaseline.x, y0);

                float dot = Mathf.Acos(Vector3.Dot(horizontal, tangent.normalized)) * 57.2957795f;
                Vector3 cross = Vector3.Cross(horizontal, tangent);
                float angle = cross.z > 0 ? dot : 360 - dot;

                matrix = Matrix4x4.TRS(new Vector3(0, y0, 0), Quaternion.Euler(0, 0, angle), Vector3.one);

                vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
                vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
                vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
                vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);

                vertices[vertexIndex + 0] += offsetToMidBaseline;
                vertices[vertexIndex + 1] += offsetToMidBaseline;
                vertices[vertexIndex + 2] += offsetToMidBaseline;
                vertices[vertexIndex + 3] += offsetToMidBaseline;
            }


            // Upload the mesh with the revised information
            m_TextComponent.UpdateVertexData();

            yield return new WaitForSeconds(SpeedMultiplier);
        }
    }

    IEnumerator AnimateVertexPositionsIV()
    {
        Matrix4x4 matrix;
        Vector3[] vertices;

        int loopCount = 0;

        // Create an Array which contains pre-computed Angle Ranges and Speeds for a bunch of characters.
        VertexAnim[] vertexAnim = new VertexAnim[1024];
        for (int i = 0; i < 1024; i++)
        {
            vertexAnim[i].angleRange = Random.Range(10f, 25f);
            vertexAnim[i].speed = Random.Range(1f, 3f);
        }

        while (loopCount < 10000)
        {
            m_TextComponent.ForceMeshUpdate();

            int characterCount = m_TextComponent.textInfo.characterCount;

            if (characterCount == 0) continue;

            for (int i = 0; i < characterCount; i++)
            {
                // Get the index of the mesh used by this character.
                int materialIndex = m_TextComponent.textInfo.characterInfo[i].materialReferenceIndex;

                vertices = m_TextComponent.textInfo.meshInfo[materialIndex].vertices;

                // Setup initial random values
                VertexAnim vertAnim = vertexAnim[i];
                TMP_CharacterInfo charInfo = m_TextComponent.textInfo.characterInfo[i];

                // Skip Characters that are not visible
                if (!charInfo.isVisible)
                    continue;

                int vertexIndex = charInfo.vertexIndex;

                Vector2 charMidTopline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.topRight.y);
                // Vector2 charMidBasline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.baseLine);

                // Need to translate all 4 vertices of each quad to aligned with middle of character / baseline.
                Vector3 offset = charMidTopline;
                // Vector3 offset = charMidBasline;

                vertices[vertexIndex + 0] += -offset;
                vertices[vertexIndex + 1] += -offset;
                vertices[vertexIndex + 2] += -offset;
                vertices[vertexIndex + 3] += -offset;

                vertAnim.angle = Mathf.SmoothStep(-vertAnim.angleRange, vertAnim.angleRange, Mathf.PingPong(loopCount / 25f * vertAnim.speed, 1f));
                Vector3 jitterOffset = new Vector3(Random.Range(-.25f, .25f), Random.Range(-.25f, .25f), 0);

                matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 0, vertexAnim[i].angle), Vector3.one);
                //matrix = Matrix4x4.TRS(jitterOffset, Quaternion.identity, Vector3.one);
                //matrix = Matrix4x4.TRS(jitterOffset, Quaternion.Euler(0, 0, Random.Range(-5f, 5f)), Vector3.one);

                vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
                vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
                vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
                vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);


                vertices[vertexIndex + 0] += offset;
                vertices[vertexIndex + 1] += offset;
                vertices[vertexIndex + 2] += offset;
                vertices[vertexIndex + 3] += offset;

                vertexAnim[i] = vertAnim;
            }

            // Push changes into meshes
            for (int i = 0; i < m_TextComponent.textInfo.meshInfo.Length; i++)
            {
                m_TextComponent.textInfo.meshInfo[i].mesh.vertices = m_TextComponent.textInfo.meshInfo[i].vertices;
                m_TextComponent.UpdateGeometry(m_TextComponent.textInfo.meshInfo[i].mesh, i);
            }

            loopCount += 1;
            yield return new WaitForSeconds(SpeedMultiplier);
        }
    }

    IEnumerator AnimateVertexPositionsV()
    {
        Matrix4x4 matrix;
        Vector3[] vertices;

        int loopCount = 0;

        // Create an Array which contains pre-computed Angle Ranges and Speeds for a bunch of characters.
        VertexAnim[] vertexAnim = new VertexAnim[1024];
        for (int i = 0; i < 1024; i++)
        {
            vertexAnim[i].angleRange = Random.Range(90f, 90f);
            vertexAnim[i].speed = Random.Range(1f, 3f);
        }

        int direction = 1;

        m_TextComponent.ForceMeshUpdate();

        while (loopCount < 10000)
        {
            //m_TextMeshPro.ForceMeshUpdate();
            //vertices = m_TextMeshPro.textInfo.meshInfo.vertices;

            int characterCount = m_TextComponent.textInfo.characterCount;

            for (int i = 0; i < characterCount; i++)
            {
                if (!m_TextComponent.textInfo.characterInfo[i].isVisible)
                    continue;

                // Get the index of the mesh used by this character.
                int materialIndex = m_TextComponent.textInfo.characterInfo[i].materialReferenceIndex;

                vertices = m_TextComponent.textInfo.meshInfo[materialIndex].vertices;

                // Setup initial random values
                VertexAnim vertAnim = vertexAnim[i];
                TMP_CharacterInfo charInfo = m_TextComponent.textInfo.characterInfo[i];

                // Skip Characters that are not visible
                if (!charInfo.isVisible)
                    continue;

                int vertexIndex = charInfo.vertexIndex;

                Vector2 charMidTopline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.topRight.y);
                // Vector2 charMidBasline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, charInfo.baseLine);

                // Need to translate all 4 vertices of each quad to aligned with middle of character / baseline.
                Vector3 offset = charMidTopline;
                // Vector3 offset = charMidBasline;

                float angle = 0;

                while (angle < 90)
                {
                    vertices[vertexIndex + 0] += -offset;
                    vertices[vertexIndex + 1] += -offset;
                    vertices[vertexIndex + 2] += -offset;
                    vertices[vertexIndex + 3] += -offset;

                    matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 15 * direction, 0), Vector3.one);

                    vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
                    vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
                    vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
                    vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);


                    vertices[vertexIndex + 0] += offset;
                    vertices[vertexIndex + 1] += offset;
                    vertices[vertexIndex + 2] += offset;
                    vertices[vertexIndex + 3] += offset;

                    // Push changes into meshes
                    for (int i2 = 0; i2 < m_TextComponent.textInfo.meshInfo.Length; i2++)
                    {
                        m_TextComponent.textInfo.meshInfo[i2].mesh.vertices = m_TextComponent.textInfo.meshInfo[i2].vertices;
                        m_TextComponent.UpdateGeometry(m_TextComponent.textInfo.meshInfo[i2].mesh, i2);
                    }

                    angle += 15;

                    yield return null;
                    //vertexAnim[i] = vertAnim;  
                }
            }

            loopCount += 1;
            direction *= -1;

            yield return new WaitForSeconds(SpeedMultiplier);
        }
    }

    IEnumerator AnimateVertexPositionsVI()
    {
        int counter = 0;
        bool loop = false;
        hasTextChanged = true;

        while (true)
        {
            // We force an update of the text object since it would only be updated at the end of the frame. Ie. before this code is executed on the first frame.
            // Alternatively, we could yield and wait until the end of the frame when the text object will be generated.
            m_TextComponent.ForceMeshUpdate();

            TMP_TextInfo textInfo = m_TextComponent.textInfo;

            int characterCount = textInfo.characterCount;

            // If No Characters then just yield and wait for some text to be added
            if (characterCount == 0)
            {
                yield return new WaitForSeconds(SpeedMultiplier);
                continue;
            }

            int visibleCount = counter % (characterCount + 1);

            m_TextComponent.maxVisibleCharacters = visibleCount;

            // Loop once all text has been revealed if allowed
            if (loop && visibleCount >= characterCount)
            {
                counter = 0;
                yield return new WaitForSeconds(SpeedMultiplier);
            }
            else if (visibleCount < characterCount)
            {
                counter += 1;
            }

            yield return new WaitForSeconds(SpeedMultiplier / 2f);
        }
    }
}
