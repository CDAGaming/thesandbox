﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text.RegularExpressions;

public class StringHandler
{
    public static string tempJsonData;

    public static string StripColors(string input)
    {
        return input == string.Empty ? input : Regex.Replace(input, "<.*?>", "");
    }

    public static string StripFilePattern(string input)
    {
        return input == string.Empty ? input : Regex.Replace(input, @"[\/\\\:\|\<>\+\""\,\?\*]", "-");
    }

    public static object GetClassObj(string assemblyName, string className)
    {
        var classObjWrapped = Activator.CreateInstance(assemblyName, className);
        return classObjWrapped.Unwrap();
    }

    /**
     * Load JSON from Parsing on Specific Classes
     */
    public static object LoadScript(object InstanceClass, string jsonLocation, bool isPlainText)
    {
        Type objectType = InstanceClass.GetType();
        if (isPlainText)
        {
            InstanceClass = JsonConvert.DeserializeObject(jsonLocation, objectType);
        }
        else
        {
            using (StreamReader r = new StreamReader(jsonLocation))
            {
                tempJsonData = r.ReadToEnd();
                InstanceClass = JsonConvert.DeserializeObject(tempJsonData, objectType);
            }
        }

        return InstanceClass;
    }

    public static void WriteScript(object jsonData, string outputLocation)
    {
        if (jsonData != null)
        {
            tempJsonData = JsonConvert.SerializeObject(jsonData, Formatting.Indented);

            using (StreamWriter writer = new StreamWriter(outputLocation))
            {
                writer.Write(tempJsonData);
            }
        }
    }
}
