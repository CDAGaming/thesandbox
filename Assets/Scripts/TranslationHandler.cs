﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TranslationHandler
{
    public static TranslationHandler Instance;
    public bool isUnicode = false;
    private string languageID = "en_us", modID;
    private Dictionary<string, string> translationMap = new Dictionary<string, string>();
    private Dictionary<string, bool> requestMap = new Dictionary<string, bool>();
    private bool usingJSON = false;

    public TranslationHandler()
    {
        SetLanguage(GlobalVariables.Instance.ConfigDataPool["MainMenu"] != null ? ((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).LanguageID : languageID);
        SetUsingJSON(false);
        GetTranslationMap();
        CheckUnicode();
    }

    public TranslationHandler(bool useJSON)
    {
        SetLanguage(GlobalVariables.Instance.ConfigDataPool["MainMenu"] != null ? ((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).LanguageID : languageID);
        SetUsingJSON(useJSON);
        GetTranslationMap();
        CheckUnicode();
    }

    public TranslationHandler(string modID)
    {
        SetLanguage(GlobalVariables.Instance.ConfigDataPool["MainMenu"] != null ? ((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).LanguageID : languageID);
        SetModID(modID);
        SetUsingJSON(false);
        GetTranslationMap();
        CheckUnicode();
    }

    public TranslationHandler(string modID, bool useJSON)
    {
        SetLanguage(GlobalVariables.Instance.ConfigDataPool["MainMenu"] != null ? ((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).LanguageID : languageID);
        SetModID(modID);
        SetUsingJSON(useJSON);
        GetTranslationMap();
        CheckUnicode();
    }

    public void Update()
    {
        if (GlobalVariables.Instance.ConfigDataPool["MainMenu"] != null && !languageID.Equals(((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).LanguageID) &&
                (!requestMap.ContainsKey(((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).LanguageID) || requestMap[((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).LanguageID]))
        {
            SetLanguage(((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).LanguageID);
            GetTranslationMap();
            CheckUnicode();
        }

        if (GlobalVariables.Instance.ConfigDataPool["MainMenu"] != null && isUnicode != ((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).ForceUnicodeDetection)
        {
            CheckUnicode();
        }
    }

    private void CheckUnicode()
    {
        isUnicode = false;
        int i = 0;
        int totalLength = 0;

        foreach (string currentString in translationMap.Values)
        {
            int currentLength = currentString.Length;
            totalLength += currentLength;

            for (int index = 0; index < currentLength; ++index)
            {
                if (currentString.ToCharArray()[index] >= 256)
                {
                    ++i;
                }
            }
        }

        float f = i / (float)totalLength;
        isUnicode = f > 0.1D || (GlobalVariables.Instance.ConfigDataPool["MainMenu"] != null && isUnicode != ((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).ForceUnicodeDetection);
    }

    private void SetUsingJSON(bool usingJSON)
    {
        this.usingJSON = usingJSON;
    }

    private void SetLanguage(string languageID)
    {
        if (languageID != string.Empty)
        {
            this.languageID = languageID;
        }
        else
        {
            this.languageID = "en_us";
        }
    }

    private void SetModID(string modID)
    {
        if (modID != string.Empty)
        {
            this.modID = modID;
        }
        else
        {
            this.modID = null;
        }
    }

    /**
     * Searches [MainDataPath]/[modID]/lang/[languageID].[json||lang] for Localization Data
     */
    private void GetTranslationMap()
    {
        string searchPath = GlobalVariables.MainDataPath + "/" + modID + "/lang/" + languageID + (usingJSON ? ".json" : ".lang");
        translationMap = new Dictionary<string, string>();

        try
        {
            if (File.Exists(searchPath))
            {
                Debug.Log("Localization found for: " + modID);
                using (StreamReader r = new StreamReader(searchPath))
                {
                    while (!r.EndOfStream)
                    {
                        string currentString = r.ReadLine();

                        if (currentString != string.Empty)
                        {
                            currentString = currentString.Trim();
                            if (!currentString.StartsWith("#") && !currentString.StartsWith("[{}]") && (usingJSON ? currentString.Contains(":") : currentString.Contains("=")))
                            {
                                string[] splitTranslation = usingJSON ? currentString.Split(":".ToCharArray(), 2) : currentString.Split("=".ToCharArray(), 2);
                                if (usingJSON)
                                {
                                    string str1 = splitTranslation[0].Substring(1, splitTranslation[0].Length - 1).Replace("\\n", "\n").Replace("\\", "").Trim();
                                    string str2 = splitTranslation[1].Substring(2, splitTranslation[1].Length - 2).Replace("\\n", "\n").Replace("\\", "").Trim();
                                    translationMap.Add(str1, str2);
                                }
                                else
                                {
                                    translationMap.Add(splitTranslation[0].Trim(), splitTranslation[1].Trim());
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Debug.Log("Translations for " + modID + " do not exist for " + languageID);
                requestMap.Add(languageID, false);
                SetLanguage("en_us");

            }
        }
        catch (Exception ex)
        {
            Debug.LogError("An Exception has Occurred while Loading Translation Mappings, Things may not work well...");
            Debug.LogException(ex);
        }
    }

    public string Translate(bool stripColors, string translationKey, params object[] parameters)
    {
        bool hasError = false;
        string translatedString = translationKey;
        try
        {
            if (translationMap.ContainsKey(translationKey))
            {
                translatedString = string.Format(translationMap[translationKey], parameters);
            }
            else
            {
                hasError = true;
            }
        }
        catch (Exception ex)
        {
            Debug.LogError("Exception Parsing " + translationKey);
            Debug.LogException(ex);
            hasError = true;
        }

        if (hasError)
        {
            Debug.LogError("Unable to retrieve a Translation for " + translationKey);
        }
        return stripColors ? StringHandler.StripColors(translatedString) : translatedString;
    }

    public string Translate(string translationKey, params object[] parameters)
    {
        return Translate(((MainMenu_Config)GlobalVariables.Instance.ConfigDataPool["MainMenu"]).StripColors, translationKey, parameters);
    }
}
