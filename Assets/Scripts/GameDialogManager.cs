﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameDialogManager : MonoBehaviour
{
    public List<Sprite> imageDisplayPool = new List<Sprite>();

    [Multiline]
    public List<string> dialogOwnerPool = new List<string>(), dialogMessagePool = new List<string>();

    public int currentDialogIndex = 0, previousDialogIndex = -1, originalEndingIndex, endingDialogIndex;

    [Multiline]
    public string currentDialogText, currentDialogOwner;

    public Sprite currentDialogImage, fallbackDialogImage;

    public Image imageData;
    public TMP_Text ownerText, messageText;
    public VertexAttributeModifier effectModifier;
    public Button proceedButton;

    public bool allowedToContinue = true;

    public void OnEnable()
    {
        originalEndingIndex = endingDialogIndex;
    }

    public void Update()
    {
        imageData.sprite = currentDialogImage;
        imageData.enabled = imageData.sprite != null;

        proceedButton.gameObject.SetActive(allowedToContinue);

        if (originalEndingIndex == 0 && endingDialogIndex != dialogMessagePool.Count)
        {
            // Update Ending Index to Message Pool Count if was not assigned to begin with
            endingDialogIndex = dialogMessagePool.Count;
        }

        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            // Transition if Enter Key is pressed also
            GoToNextDialogIndex();
        }

        if (currentDialogIndex != previousDialogIndex && currentDialogIndex < endingDialogIndex)
        {
            // Update Dialog Data if Index Changes
            currentDialogOwner = (dialogOwnerPool != null && currentDialogIndex <= dialogOwnerPool.Count) ? dialogOwnerPool[currentDialogIndex] : "";
            currentDialogText = (dialogMessagePool != null && currentDialogIndex <= dialogMessagePool.Count) ? dialogMessagePool[currentDialogIndex] : "";
            currentDialogImage = (imageDisplayPool != null && currentDialogIndex < imageDisplayPool.Count) ? imageDisplayPool[currentDialogIndex] : (GlobalVariables.Instance.spritePool.ContainsKey(currentDialogOwner.Replace(" ", "").Trim()) ? GlobalVariables.Instance.spritePool[currentDialogOwner.Replace(" ", "").Trim()] : null);

            ownerText.text = currentDialogOwner;
            messageText.text = currentDialogText;
            effectModifier.enabled = true;
        }
        else if (currentDialogIndex == endingDialogIndex)
        {
            Destroy(gameObject);
        }
    }

    public void GoToNextDialogIndex()
    {
        if (allowedToContinue)
        {
            previousDialogIndex = currentDialogIndex;
            currentDialogIndex++;

            effectModifier.enabled = false;
        }
    }

    public void GoToPreviousDialogIndex()
    {
        if (allowedToContinue)
        {
            previousDialogIndex = currentDialogIndex;
            currentDialogIndex--;

            effectModifier.enabled = false;
        }
    }
}
