﻿using UnityEngine;

public class MainMenu_Config
{
    // Last Version this Config was On
    // If Config Issues happen to occur, Please Refer to this ID
    public string VersionID { get; set; } = Application.version;
    // During Translating Strings, if this is enabled
    // <color> and other formatting HTML tags
    // Will be removed from the resulting String before being received
    public bool StripColors { get; set; } = false;
    // Forces the Translator to Use Unicode Detection
    // This was mostly used for Rendering Purposes for CraftPresence
    // But Does not yet have usage in C#
    public bool ForceUnicodeDetection { get; set; } = false;
    // Language ID to refer to when the App translates Strings
    // Language ID's should be all lowercase with an underscore in the middle
    // Example: "en_us" is equal to "English (United States)"
    public string LanguageID { get; set; } = "en_us";
    // This will switch the translator to utilize a .json file
    // For Localizing rather then the .lang format
    // This is by Default true for C#, to reduce strain on users
    public bool UseJsonLanguageFile { get; set; } = true;
    // Controls the Music Volume // Volume for the Main Audio Source
    public float MusicVolume { get; set; } = 0.85f;
    // Controls the Volume for Extra Effects for Players, Enemies, etc.
    public float SFXVolume { get; set; } = 0.6f;


    public static string rawData;
}
