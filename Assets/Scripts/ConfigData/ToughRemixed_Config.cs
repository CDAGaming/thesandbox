﻿public class ToughRemixed_Config
{
    // Allows Flash Effects in Levels (Epilepsy Warning With This)
    public bool EnableFlashes { get; set; } = true;
    // Allows Online (Other Alternative Universe Nando) the opportunity
    // To enter your story mode game, making it slightly harder
    public bool AllowOnlineCollisionWithStory { get; set; } = false;

    // Used to access the BeatSaber Endpoint for Song Retreival
    public string BeatSaberURL { get; set; } = "https://beatsaver.com";
    // Used to access Doucl Services for Song Retreival alongside Beat Saver
    public string DouclURL { get; set; } = "https://api.doucl.com";


    public static string rawData;
}
