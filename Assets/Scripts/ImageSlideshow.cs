using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Defines a List of Images to Scroll through
 * Controlled via a configurable Timer
 */
public class ImageSlideshow : MonoBehaviour
{
    public float timeUntilSwitch;
    public int currentImageIndex = 0, countUntilCompletion = -1;
    public string ImageID;
    public bool isFinished, isAnimation, scaleTransformSize = true, scaleColliderSize = true, resetUponDisable = true;
    public List<Sprite> spriteList = new List<Sprite>();
    public Image rootObject;

    public RectTransform objectTransform;
    public BoxCollider objectCollider;

    private int originalCompletionCount = -1;

    public void OnEnable()
    {
        originalCompletionCount = countUntilCompletion;

        rootObject = GlobalVariables.Instance.GetOrAddComponent(gameObject, rootObject);
        objectTransform = GlobalVariables.Instance.GetOrAddComponent(gameObject, objectTransform);
        objectCollider = scaleColliderSize ? GlobalVariables.Instance.GetOrAddComponent(gameObject, objectCollider) : GetComponent<BoxCollider>();

        SetupImageData();
    }

    public void SetupImageData()
    {
        // Set Sprite List (and Initial Image) Dynamically Based on Menu and Sprite Pool Stored
        if (GlobalVariables.Instance.spritePool != null && GlobalVariables.Instance.spritePool.Count != 0)
        {
            if (ImageID != string.Empty)
            {
                if (isAnimation)
                {
                    // Begin Cycle Image CoRoutine
                    StartCoroutine(CycleImages());

                    bool finishedCounting = false;

                    // Attempt to add all Images of an Animation
                    // Format: AnimationID_frameNumber
                    while (!finishedCounting)
                    {
                        try
                        {
                            spriteList.Add(GlobalVariables.Instance.spritePool[ImageID + "_" + spriteList.Count]);
                        }
                        catch (Exception)
                        {
                            Debug.Log("Finished Getting animations for " + ImageID + " at " + spriteList.Count);
                            finishedCounting = true;
                        }
                    }

                    // Set Initial Image to the Current Image Index
                    SetImageSprite(spriteList[currentImageIndex]);
                }
                else
                {
                    // Attempts to Locate Image just based on the ImageID
                    if (GlobalVariables.Instance.spritePool.ContainsKey(ImageID))
                    {
                        SetImageSprite(GlobalVariables.Instance.spritePool[ImageID]);
                    }
                    else
                    {
                        Debug.Log("No Images were found for " + ImageID);
                    }
                }
            }
            else
            {
                Debug.Log("Please Enter an ImageID or remove this component If not needed...");
            }
        }
        else
        {
            Debug.Log("No Sprites are available in the Sprite Pool, Please Report this if this is a mistake...");
        }
    }

    public void SetImageSprite(Sprite targetSprite)
    {
        rootObject.sprite = targetSprite;

        if (scaleTransformSize)
        {
            objectTransform.sizeDelta = new Vector2(targetSprite.rect.width, targetSprite.rect.height);
        }

        if (scaleColliderSize && objectCollider != null)
        {
            objectCollider.size = new Vector3(targetSprite.rect.width, targetSprite.rect.height, objectCollider.size.z);
        }
    }

    public IEnumerator CycleImages()
    {
        while (!isFinished)
        {
            yield return new WaitForSeconds(timeUntilSwitch);

            if (currentImageIndex >= spriteList.Count)
            {
                if (countUntilCompletion != -1 && countUntilCompletion == 0)
                {
                    isFinished = true;
                }
                else
                {
                    if (countUntilCompletion > 0)
                    {
                        countUntilCompletion--;
                    }
                    currentImageIndex = 0;
                }
            }

            // Double-Protection to avoid any issues with isFinished
            if (!isFinished)
            {
                if (spriteList != null && spriteList.Count != 0 && spriteList[currentImageIndex] != null)
                {
                    SetImageSprite(spriteList[currentImageIndex]);
                }
                currentImageIndex++;
            }
        }
    }

    public void OnDisable()
    {
        // Reset Data
        StopAllCoroutines();

        if (resetUponDisable)
        {
            isFinished = false;
            currentImageIndex = 0;
            countUntilCompletion = originalCompletionCount;

            spriteList.Clear();
        }
    }
}
