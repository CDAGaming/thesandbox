﻿using System.Collections;
using UnityEngine;

/**
 * This Script is called for Modules that have specific transitions to other GameObjects (Example: Warnings)
 * Main Task has it wait a certain amount of time (Configurable) before Continuing to a specified GameObject and Disabling the called GameObject
 */
public class ObjectTransitionScript : MonoBehaviour
{
    public GameObject waitObject, nextObject;
    public bool hookIntoMenuSystem;
    public float secondsTimeToWait;
    public void OnEnable()
    {
        StartCoroutine(WaitToShiftFrame());
    }

    public IEnumerator WaitToShiftFrame()
    {
        yield return new WaitForSeconds(secondsTimeToWait);

        if (hookIntoMenuSystem)
        {
            GlobalVariables.Instance.NewSubMenu = nextObject.name;
        }
        else
        {
            if (nextObject != null)
            {
                nextObject.SetActive(true);
            }
            waitObject.SetActive(false);
        }
    }
}
