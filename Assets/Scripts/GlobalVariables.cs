using JetFistGames.RetroTVFX;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net.NetworkInformation;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class GlobalVariables : MonoBehaviour
{
    public static GlobalVariables Instance;
    public static string MainDataURL, MainDataPath, PersistentDataPath;

    public string ApplicationTitle, ApplicationVersion, CurrentMenu, NewMenu, PreviousMenu, CurrentSubMenu, PreviousSubMenu, NewSubMenu;
    public GameObject[] gameObjects;
    public GameObject CurrentMenuObj, CurrentSubMenuObj, AudioSRCObj;

#if UNITY_STANDALONE || UNITY_EDITOR
    public DiscordManager discordManager;
#endif

    public CanvasScaler canvasScaler;
    public EventSystem eventSystem;
    public Sprite tempSprite;
    public Texture2D tempTexture;
    public TranslationHandler tempTranslator;

    public Dictionary<string, Sprite> spritePool = new Dictionary<string, Sprite>();
    public Dictionary<string, CanvasGroup> UIGameObjectPool = new Dictionary<string, CanvasGroup>(), SubUIGameObjects = new Dictionary<string, CanvasGroup>();
    public Dictionary<string, TranslationHandler> TranslatorPool = new Dictionary<string, TranslationHandler>();
    public Dictionary<string, object> ConfigDataPool = new Dictionary<string, object>();
    public Dictionary<string, bool> DownloadPool = new Dictionary<string, bool>();

    // Loading Data for Downloading Module Data
    private bool isRetreivingData = false, downloadCompleted = false;
    public GameObject loadingObject;

    // Prefab Dictionary
    public GameObject gameDialogObj;

    // Camera Bounds Data
    public Camera mainCamera;
    public Vector2 mainCameraBounds;

    public SongData currentSongData;

    // Effect Data
    public CRTEffect crtEffectManager;
    public FadeEffect fadeEffectManager;
    public VignetteAndChromaticAberration vignetteEffectManager;
    public AudioHumEffect humEffectManager;

    public void Awake()
    {
        // Scale Canvas to Current Resolution, as needed
        // TODO: Move this to a Reload Function when Options Menu is available
        canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

        // Update Allowed Camera Boundaries
        // TODO: Move to better Function
        mainCamera = Camera.main;
        mainCameraBounds = new Vector2(canvasScaler.referenceResolution.x / 2, canvasScaler.referenceResolution.y / 2);

        ApplicationTitle = Application.productName;
        ApplicationVersion = Application.version;
        NewMenu = "MainMenu";

        Instance = this;
        MainDataURL = "https://github.com/CDAGaming/VersionLibrary/raw/master/Unity/" + ApplicationTitle.Replace(" ", "") + "/";
        PersistentDataPath = Application.persistentDataPath + "/Data";
        MainDataPath = PersistentDataPath + "/v" + Application.version;

#if UNITY_STANDALONE || UNITY_EDITOR
        // Setup Initial Rich Precence, if connected to internet
        //discordManager.gameObject.SetActive(IsConnectedToInternet());
#endif

        // Get UI GameObjects for available modules and sub objects
        gameObjects = Resources.FindObjectsOfTypeAll<GameObject>();
        foreach (GameObject gameObj in gameObjects)
        {
            if (gameObj.GetComponent<CanvasGroup>() != null)
            {
                CanvasGroup targetGroup = gameObj.GetComponent<CanvasGroup>();
                if (gameObj.layer == 12 && !UIGameObjectPool.ContainsValue(targetGroup) && !UIGameObjectPool.ContainsKey(gameObj.name))
                {
                    UIGameObjectPool.Add(gameObj.name, targetGroup);
                    CreateDataDir(gameObj.name);
                }
                else if (gameObj.layer == 9 && !SubUIGameObjects.ContainsValue(targetGroup) && !SubUIGameObjects.ContainsKey(gameObj.name))
                {
                    SubUIGameObjects.Add(gameObj.name, targetGroup);
                }
            }
        }

        // Set Audio Source Game Object activity, after Initialization
        AudioSRCObj.SetActive(true);

        // Remove older Versions Assets, if others exist
        foreach (string filePathOG in Directory.GetDirectories(PersistentDataPath, "*.*", SearchOption.TopDirectoryOnly))
        {
            string filePath = filePathOG.Replace("\\", "/").Trim();
            if (!filePath.StartsWith(MainDataPath) && filePath.StartsWith("v"))
            {
                Debug.Log("Deleting Old Version Data from: " + filePath);
                Directory.Delete(filePath, true);
            }
        }
    }

    public void Update()
    {
        // Update Instance as needed
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }

        // Scale Canvas to Current Resolution, as needed
        // TODO: Move this to a Reload Function when Options Menu is available
        canvasScaler.referenceResolution = new Vector2(Screen.width, Screen.height);

        // Update Allowed Camera Boundaries
        // TODO: Move to better Function
        mainCamera = Camera.main;
        mainCameraBounds = new Vector2(canvasScaler.referenceResolution.x / 2, canvasScaler.referenceResolution.y / 2);

        // On Update, Change the CurrentMenu, if there is an Update
        if (NewMenu != CurrentMenu)
        {
            if (CurrentMenu != string.Empty && UIGameObjectPool.ContainsKey(CurrentMenu))
            {
                // Fade out the Old UI
                FadeUIOut(UIGameObjectPool[CurrentMenu], false);
            }

            if (!isRetreivingData)
            {
                // Fade In Loading Screen, if any
                if (loadingObject != null)
                {
                    if (loadingObject.GetComponent<CanvasGroup>() != null)
                    {
                        FadeUIIn(loadingObject.GetComponent<CanvasGroup>(), null);
                    } else
                    {
                        loadingObject.SetActive(true);
                    }
                }

                // Check for any present Module Data and Download if able to
                try
                {
                    StartCoroutine(DownloadFile(NewMenu, MainDataURL + NewMenu + "/" + NewMenu + ".zip", MainDataPath + "/" + NewMenu + "/" + NewMenu + ".zip", true));
                }
                catch (Exception)
                {
                    Debug.LogError("Unable to Download Module files for " + NewMenu + ", will try to continue, but expect many issues to occur in the module...");
                    downloadCompleted = true;
                }
            }

            if (downloadCompleted)
            {
                // Fade Out Loading Screen, if any
                if (loadingObject != null)
                {
                    if (loadingObject.GetComponent<CanvasGroup>() != null)
                    {
                        FadeUIOut(loadingObject.GetComponent<CanvasGroup>(), false);
                    }
                    else
                    {
                        loadingObject.SetActive(false);
                    }
                }

                if (NewMenu != string.Empty && UIGameObjectPool.ContainsKey(NewMenu))
                {
                    // Fade In the New UI
                    FadeUIIn(UIGameObjectPool[NewMenu], "main");

                    Debug.Log("Changed Active Menu from " + (CurrentMenu != string.Empty ? CurrentMenu : "None") + " to " + (NewMenu != string.Empty ? NewMenu : "None"));
                    CurrentMenu = NewMenu;

#if UNITY_STANDALONE || UNITY_EDITOR
                    discordManager.details = "In Menu: " + CurrentMenu;
                    discordManager.UpdatePresence();
#endif
                }

                // Reset Values
                downloadCompleted = false;
                isRetreivingData = false;
            }
        }

        // Update Sub Menu
        if (NewSubMenu != CurrentSubMenu)
        {
            if (CurrentSubMenuObj != null && SubUIGameObjects.ContainsKey(CurrentSubMenu))
            {
                // Fade Out Old UI
                FadeUIOut(SubUIGameObjects[CurrentSubMenu], false);
            }

            if (NewSubMenu != string.Empty && SubUIGameObjects.ContainsKey(NewSubMenu))
            {
                // Fade In the New UI
                FadeUIIn(SubUIGameObjects[NewSubMenu], "sub");

                Debug.Log("Changed Active Sub-Menu from " + (CurrentSubMenu != string.Empty ? CurrentSubMenu : "None") + " to " + (NewSubMenu != string.Empty ? NewSubMenu : "None"));
                CurrentSubMenu = NewSubMenu;

#if UNITY_STANDALONE || UNITY_EDITOR
                discordManager.state = "In Sub Menu: " + CurrentSubMenu;
                discordManager.UpdatePresence();
#endif
            }
        }

        // Update Localization Engines on each Update
        // (Mostly doing this as a workaround for MonoBehavior Warnings for new TranslationHandler() Instancing)
        foreach (TranslationHandler translatorObj in TranslatorPool.Values)
        {
            translatorObj.Update();
        }
    }

    public void LateUpdate()
    {
        // Event System Hooks to prevent Random Issues, such as a button being stuck on Selected but user drags the mouse off the button
        if (eventSystem != null)
        {
            if (eventSystem.currentSelectedGameObject != null && eventSystem.currentSelectedGameObject.GetComponent<Button>() != null && !Input.GetMouseButton(0))
            {
                StartCoroutine(ExecuteButton());
            }
        }
    }

    public void FadeUIIn(CanvasGroup UI, string menuID)
    {
        StartCoroutine(UIFadeIn(UI, menuID));
    }

    public void FadeUIOut(CanvasGroup UI, bool setActive)
    {
        StartCoroutine(UIFadeOut(UI, setActive));
    }

    public IEnumerator UIFadeIn(CanvasGroup UI, string menuID)
    {
        while (UI.alpha < 1)
        {
            yield return new WaitForEndOfFrame();
            UI.alpha += Time.deltaTime * 5;
        }
        UI.gameObject.SetActive(true);

        if (menuID != string.Empty)
        {
            if (menuID.ToLower() == "main")
            {
                CurrentMenuObj = UI.gameObject;
            }
            else if (menuID.ToLower() == "sub")
            {
                CurrentSubMenuObj = UI.gameObject;
            }
            else
            {
                Debug.Log("Invalid MenuID was called during transition, ignoring...");
            }
        }
    }

    public IEnumerator UIFadeOut(CanvasGroup UI, bool setActive)
    {
        while (UI.alpha > 0)
        {
            yield return new WaitForEndOfFrame();
            UI.alpha -= Time.deltaTime * 5;
        }
        UI.gameObject.SetActive(setActive);
    }

    public IEnumerator ExecuteButton()
    {
        yield return new WaitForEndOfFrame();

        string formattedMenuName = eventSystem.currentSelectedGameObject.name.Replace("Button", "");
        if (UIGameObjectPool.ContainsKey(formattedMenuName))
        {
            PreviousMenu = CurrentMenu;
            NewMenu = formattedMenuName;
        }
        else if (formattedMenuName == "Back" && UIGameObjectPool.ContainsKey(PreviousMenu))
        {
            NewMenu = PreviousMenu;
        }
        else if (SubUIGameObjects.ContainsKey(formattedMenuName))
        {
            PreviousSubMenu = CurrentSubMenu;
            NewSubMenu = formattedMenuName;
        }
        else if (formattedMenuName == "SubBack" && SubUIGameObjects.ContainsKey(PreviousSubMenu))
        {
            NewSubMenu = PreviousSubMenu;
        }
        else
        {
            // Unimplemented if it Reaches here (Uncomment for subsequent Log Spam :P)
            //Debug.LogError("Error: Unimplemented Function for " + formattedMenuName);
        }

        eventSystem.SetSelectedGameObject(null);
    }

    public void Exit()
    {
        // Checks if Application isEditor, otherwise do normal Quit via Application.Exit() if not or it failed
        if (!Application.isEditor || !SetMethodData("UnityEditor.EditorApplication", "isPlaying", false))
        {
            Application.Quit();
        }
    }

    /**
     * Retreives or Adds a Component
     * Mostly used if a Component is required or needs to be checked on
     * Target Componentshould be the component variable your assigning too, which the below statment is equivalent to a coalesce statement
     * 
     * Note:
     * The Diamond Generic Operator is not needed in the call if
     * you are specifying a targetComponent, where it'll get the type from that instead
     * unless specified otherwise by the operator
     */
    public T GetOrAddComponent<T>(GameObject targetObject, T targetComponent) where T : Component
    {
        if (targetObject == null)
        {
            targetObject = gameObject;
        }

        T component;

        if (targetComponent != null)
        {
            component = targetComponent;
        }
        else
        {
            component = targetObject.GetComponent<T>();
        }

        if (component == null)
            component = targetObject.AddComponent<T>();

        return component;

    }

    public bool SetMethodData(string methodInstance, string property, object value)
    {
        // Casts a Methods Property via Reflection
        // Return True if Success, False Otherwise
        foreach (Assembly assemblyObj in AppDomain.CurrentDomain.GetAssemblies())
        {
            Type typeObj = assemblyObj.GetType(methodInstance);
            if (typeObj != null)
            {
                typeObj.GetProperty(property).SetValue(null, value, null);
                return true;
            }
        }
        return false;
    }

    public IEnumerator DownloadFile(string downloadID, string downloadUrl, string downloadLocation, bool importAsModule)
    {
        isRetreivingData = true;

        string[] splitUrl = downloadLocation.Split("/".ToCharArray());
        string fileName = splitUrl[splitUrl.Length - 1];

        if (DownloadPool.ContainsKey(downloadID) && DownloadPool[downloadID])
        {
            DownloadPool.Remove(downloadID);
        }

        // Step 1: Download the File via a UnityWebRequest
        if ((!DownloadPool.ContainsKey(downloadID)) && !File.Exists(downloadLocation) && Directory.GetFiles(downloadLocation.Replace(fileName, "")).Length == 0 && Directory.GetDirectories(downloadLocation.Replace(fileName, "")).Length == 0)
        {
            DownloadPool.Add(downloadID, false);
            Debug.Log("Downloading " + downloadUrl + " to " + downloadLocation);

            DownloadHandlerFile fileDownloader = new DownloadHandlerFile(downloadLocation);
            UnityWebRequest www = new UnityWebRequest(downloadUrl, "GET", fileDownloader, null);

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Download was Successfully Completed for " + fileName);
                DownloadPool[downloadID] = true;
            }
            www.Dispose();
        }
        else
        {
            Debug.Log("Error: Location/File already exists, download may have been done before for " + fileName + "...");
        }

        // Step 2: File Extension Interpreting (Ex: Extract Zip, if Zip was downloaded or is present in the folder)
        if (File.Exists(downloadLocation))
        {
            Debug.Log("Now Interpreting file extension for " + fileName);

            if (fileName.EndsWith(".zip"))
            {
                Debug.Log("Recognized " + fileName + " as ZIP, Extracting Data...");
                ZipFile.ExtractToDirectory(downloadLocation, downloadLocation.Replace(fileName, ""));
                Debug.Log("Extraction Completed for " + fileName);
            }

            // Delete the original Zip after Extraction
            File.Delete(downloadLocation);
        }

        // Step 3: Assign Data to Pools based upon Assets available, if importing as a module
        if (importAsModule)
        {
            foreach (string filePathOG in Directory.GetFiles(downloadLocation.Replace(fileName, ""), "*.*", SearchOption.AllDirectories))
            {
                string filePath = filePathOG.Replace("\\", "/").Trim();

                string[] splitPath = filePath.Split("/".ToCharArray());

                string pathFileName = splitPath[splitPath.Length - 1].Split(".".ToCharArray())[0];
                string pathFileExtension = splitPath[splitPath.Length - 1].Split(".".ToCharArray())[1];

                // Use Image Utility to Convert to Sprite // Texture2D (From PSInteractive)
                if (filePath.EndsWith(".png") || filePath.EndsWith(".jpg") || filePath.EndsWith(".jpeg"))
                {
                    tempTexture = IMG2Sprite.LoadTexture(filePath);
                    tempTexture.name = pathFileName;

                    tempTexture.filterMode = FilterMode.Point;

                    tempSprite = IMG2Sprite.ConvertTextureToSprite(tempTexture);

                    if (!spritePool.ContainsValue(tempSprite) && !spritePool.ContainsKey(pathFileName))
                    {
                        Debug.Log("Added Image to Sprite Pool as: " + pathFileName);
                        spritePool.Add(pathFileName, tempSprite);
                    }

                    tempSprite = null;
                    tempTexture = null;
                }
                else if (filePath.EndsWith(".mp3"))
                {
                    AudioManager.Instance.ConvertMp3ToWav(filePath, filePath.Replace(".mp3", ".wav"));
                    File.Delete(filePath);
                    StartCoroutine(AudioManager.Instance.GetAudioClip(pathFileName, new Uri(filePath.Replace(".mp3", ".wav")).AbsoluteUri, AudioType.WAV, true));
                }
                else if (filePath.EndsWith(".wav"))
                {
                    StartCoroutine(AudioManager.Instance.GetAudioClip(pathFileName, new Uri(filePath).AbsoluteUri, AudioType.WAV, true));
                }
                else if (filePath.EndsWith(".lang") || filePath.EndsWith(".json"))
                {
                    // Assume that there is Localization, if after Downloading Assets, a .lang or .json file exists
                    tempTranslator = new TranslationHandler(CurrentMenu, ((MainMenu_Config)ConfigDataPool["MainMenu"]).UseJsonLanguageFile);

                    if (!TranslatorPool.ContainsValue(tempTranslator) && !TranslatorPool.ContainsKey(CurrentMenu))
                    {
                        TranslatorPool.Add(CurrentMenu, tempTranslator);
                    }
                    tempTranslator = null;
                }
                else
                {
                    Debug.LogError("Error: Unsupported File Detected, and will not be imported! (Name: " + pathFileName + "." + pathFileExtension + ")");
                }
            }
        }

        downloadCompleted = true;
    }

    /**
     * Creates a Directory for a Module in the Applications Data Path
     * Root Assets Folder while in the Editor
     * %appdata%/../LocalLow Directory while in Runtime
     */
    public void CreateDataDir(string moduleName)
    {
        if (!Directory.Exists(MainDataPath + "/" + moduleName))
        {
            Directory.CreateDirectory(MainDataPath + "/" + moduleName);
            File.SetAttributes(MainDataPath + "/" + moduleName, FileAttributes.Directory);
        }
        else
        {
            Debug.Log("A Data Directory for " + moduleName + " already exists, continuing...");
        }

        // Also Create or Read a Config File for the module
        object classObj = null;

        try
        {
            classObj = StringHandler.GetClassObj(null, moduleName + "_Config");
        }
        catch (Exception)
        {
            Debug.Log("Config Parsing for " + moduleName + " does not exist, Continuing...");
        }

        if (classObj != null)
        {
            if (!File.Exists(PersistentDataPath + "/" + moduleName + "_Config.json"))
            {
                Debug.Log("Config does not exist for " + moduleName + ", Creating...");

                FileStream fs = File.Create(PersistentDataPath + "/" + moduleName + "_Config.json");
                fs.Close();

                StringHandler.WriteScript(classObj, PersistentDataPath + "/" + moduleName + "_Config.json");
                Debug.Log("Created Config Data for " + moduleName);
            }
            else
            {
                Debug.Log("Reading Config Data from " + moduleName);
                classObj = StringHandler.LoadScript(classObj, PersistentDataPath + "/" + moduleName + "_Config.json", false);
            }
            ConfigDataPool.Add(moduleName, classObj);
        }
    }

    /**
     * Attempts to check Internet Connectivity via a Ping or a UnityWebRequest to Google Resources
     */
    public bool IsConnectedToInternet()
    {
        try
        {
            using (System.Net.NetworkInformation.Ping myPing = new System.Net.NetworkInformation.Ping())
            {
                string host = "8.8.8.8";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
        }
        catch (Exception)
        {
            try
            {
                // For a Fallback, Try a Read on Google's 254 Response Page via a GET UnityWebRequest
                UnityWebRequest www = UnityWebRequest.Get("http://clients3.google.com/generate_204");
                www.SendWebRequest();

                bool isOk = !www.isNetworkError && !www.isHttpError;
                www.Dispose();
                return isOk;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    /**
     * Rotates a GameObject by the specified Rotation over a set time in a specified direction
     * Tip: Use Vector3.right or transform.right (Local Direction) for right, and so on
     */
    public IEnumerator Rotate(Transform objectTransform, float duration, float rotation, Vector3 direction)
    {
        Quaternion startRot = objectTransform.rotation;
        float t = 0.0f;
        while (t < duration)
        {
            t += Time.deltaTime;
            objectTransform.rotation = startRot * Quaternion.AngleAxis(t / duration * rotation, direction);
            yield return null;
        }
        objectTransform.rotation = startRot;
    }
}
