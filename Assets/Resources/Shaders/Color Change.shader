// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Color Change"
{
	Properties
	{
		_RedReplace("Red Replace", Color) = (1,0.9140323,0,1)
		_GreenReplace("Green Replace", Color) = (1,0.9140323,0,1)
		_BlueReplace("Blue Replace", Color) = (1,0.6825919,0,1)
		_MainTex("Main Tex", 2D) = "white" {}
		_Tiling("Tiling", Vector) = (1,1,0,0)
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}
	
	SubShader
	{
		
		
		Tags { "RenderType"="Transparent" }
		LOD 100

		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend SrcAlpha OneMinusSrcAlpha
		AlphaToMask On
		Cull Back
		ColorMask RGBA
		ZWrite On
		ZTest LEqual
		Offset 0 , 0
		
		
		
		Pass
		{
			Name "Unlit"
			
			CGPROGRAM

#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
		//only defining to not throw compilation error over Unity 5.5
		#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				float4 ase_texcoord : TEXCOORD0;
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord : TEXCOORD0;
			};

			uniform sampler2D _TextureSample0;
			uniform float4 _TextureSample0_ST;
			uniform float4 _BlueReplace;
			uniform sampler2D _MainTex;
			uniform float2 _Tiling;
			uniform float4 _RedReplace;
			uniform float4 _GreenReplace;
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				o.ase_texcoord.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord.zw = 0;
				float3 vertexValue =  float3(0,0,0) ;
				#if ASE_ABSOLUTE_VERTEX_POS
				v.vertex.xyz = vertexValue;
				#else
				v.vertex.xyz += vertexValue;
				#endif
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
				fixed4 finalColor;
				float2 uv_TextureSample0 = i.ase_texcoord.xy * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
				float4 tex2DNode17 = tex2D( _TextureSample0, uv_TextureSample0 );
				float2 uv026 = i.ase_texcoord.xy * _Tiling + float2( 0,0 );
				float4 tex2DNode3 = tex2D( _MainTex, uv026 );
				float4 lerpResult24 = lerp( tex2DNode17 , ( _BlueReplace * tex2DNode17 ) , tex2DNode3.b);
				float4 lerpResult21 = lerp( tex2DNode17 , ( tex2DNode17 * _RedReplace ) , tex2DNode3.r);
				float4 lerpResult30 = lerp( tex2DNode17 , ( tex2DNode17 * _GreenReplace ) , tex2DNode3.g);
				
				
				finalColor = ( ( lerpResult24 * lerpResult21 * lerpResult30 ) * tex2DNode3.a );
				return finalColor;
			}
			ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	Fallback "Diffuse"
}
/*ASEBEGIN
Version=16800
1601;138;1358;745;1032.605;1187.228;2.271664;True;True
Node;AmplifyShaderEditor.Vector2Node;27;-1814.459,-66.00266;Float;False;Property;_Tiling;Tiling;4;0;Create;True;0;0;False;0;1,1;1.2,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.ColorNode;22;-1043.931,-831.0399;Float;False;Property;_BlueReplace;Blue Replace;2;0;Create;True;0;0;False;0;1,0.6825919,0,1;0,0,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;17;-1595.641,-461.0103;Float;True;Property;_TextureSample0;Texture Sample 0;5;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;18;-1359.074,-201.1523;Float;False;Property;_RedReplace;Red Replace;0;0;Create;True;0;0;False;0;1,0.9140323,0,1;1,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;28;-758.5088,320.0799;Float;False;Property;_GreenReplace;Green Replace;1;0;Create;True;0;0;False;0;1,0.9140323,0,1;0,1,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;26;-1492.797,19.54716;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-367.92,318.0616;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-808.5544,-612.8016;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;3;-1098.138,73.27039;Float;True;Property;_MainTex;Main Tex;3;0;Create;True;0;0;False;0;None;c979a6a26d00fd4408b77bed6cbba172;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-930.2264,-275.9198;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;21;-463.8853,-422.4659;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;30;-19.79877,-177.3828;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;24;-516.5606,-999.7756;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;280.7455,-417.4631;Float;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;700.4816,-300.7663;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;14;1096.674,-354.5296;Float;False;True;2;Float;ASEMaterialInspector;0;1;Color Change;0770190933193b94aaa3065e307002fa;True;Unlit;0;0;Unlit;2;True;2;5;False;-1;10;False;-1;0;5;False;-1;10;False;-1;True;0;False;-1;0;False;-1;True;True;True;0;False;-1;True;True;True;True;True;0;False;-1;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;RenderType=Transparent=RenderType;True;2;0;False;False;False;False;False;False;False;False;False;True;0;False;0;Diffuse;0;0;Standard;1;Vertex Position,InvertActionOnDeselection;1;0;1;True;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;26;0;27;0
WireConnection;29;0;17;0
WireConnection;29;1;28;0
WireConnection;23;0;22;0
WireConnection;23;1;17;0
WireConnection;3;1;26;0
WireConnection;19;0;17;0
WireConnection;19;1;18;0
WireConnection;21;0;17;0
WireConnection;21;1;19;0
WireConnection;21;2;3;1
WireConnection;30;0;17;0
WireConnection;30;1;29;0
WireConnection;30;2;3;2
WireConnection;24;0;17;0
WireConnection;24;1;23;0
WireConnection;24;2;3;3
WireConnection;25;0;24;0
WireConnection;25;1;21;0
WireConnection;25;2;30;0
WireConnection;35;0;25;0
WireConnection;35;1;3;4
WireConnection;14;0;35;0
ASEEND*/
//CHKSM=B425599574BBD57779D0D5BDD80E85197D386A7B