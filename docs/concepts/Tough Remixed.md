## Tough Remixed - Conceptual Design (Version 2.1.0)
Characters:
 - Little Nando (Has rendition-based powers, and powers that grow stronger as the game continues)
 - Various Degree of Enemies, from the Newer and Older Revisions of TC's Universe, spawned by Nando, as well as new ones
 - Cody + Including the other 3 characters (Who will aid this new player on his way)
 - A New Player, created from the Mystery Entity 5 years before in Live IV's timeline, Player ripped from Cody's Control in Intro
   - In the secret ending, it'll show the mystery entity coming through the tear and recovering the Player's Control Essence, that is implanted into a new host body with hidden programming
   - The new body will spend the years before Remixed, as per their programming, gaining the trust of others, leading into the initial remixed events
 - A Mystery Enemy (Mysterious Entity who appeared from a Dimensional Tear and the direct cause of Remixed's Events)
 
Plot:
 - Game has a Story Mode + Remixed LIVE Mode (Story will also possibly have an Insane Mode)
 - Intro begins in late 2017 during the end of Sarmiento's Reign in Tough Coded Live IV's timeline.
   - A final conflict is probable to happen between all 4 of the main characters from Live IV with Nando (Mysteriously reactivated after Sarmiento's departure)
   - The conflict will give you control over Cody, as per the game rules in Live IV's timeline
   - The fight and supposed "finale", entitled fake finale, involves Nando short circuiting and deactivating, but not before letting out a massive energy explosion
   - The resulting explosion tears out the controllable Player's control from Cody, giving him his own personality back in comparison to the player's control of them
 - Story takes place in the present day with Guiding the player through a few tutorial levels guiding the player through how to move, and how to dodge obstacles (As well as using a shield to bounce back obstacles and destroy them)
   - These Levels are expected to be easy, giving the User a false sense of simplicity to the game (Noone should die in these, even on hard mode)
   - At the end of the tutorial, the player fights an older rendition of Nando (one with significantly lesser powers, comparable to the original LIVE days)
   - While other enemies will flash and explode, the older nando fight will see him flash but rather fade away instead of dying
 - After the tutorial, the player is taken through a representation of the peace Nando's Realm has had in his absence (4 years have gone by since the continuous battles of TC: Live IV)
   - In reality, Nando has spent years preparing his final takeover -- Not in any of his prior forms, but a stronger and continuously evolving forms powered from the stars
   - During the players tour, Nando breaks free from where the tutorials took place, and begins to re-take over the realm to his very wishes and desire
   - In this realm, power stars have rained from the sky at some times, which can both give the Player OR Nando more power and abilities (Though Nando has more experience with them as they were used in his original days to evolve)
   - After absorbing hundreds if not millions of these stars from both the realm and the sky as they fall, Nando morphs THROUGH his older renditions, though his Current one in TC Live IV as well as the one in ReCoded, and
     becomes much more menacing, bigger, and stronger then the player, while also unleashing hordes of enemies and danger across the realm.
	 
   - The Player ventures on in his new found quest: To Save the Realm once and for all that Nando has now full control over
     - Along the way, The Player's path is both littered with bobby traps as well as Levels featuring both new and old enemies working alongside each other, powered by Nando's new capabilities
	 - The Player also along the way meets Cody -- now a tad weary from his prior battles with Nando over the years (Either in very late game, or during the final battle), giving the New Player a weapon to finally attack and eventually strike down Nando
	 
   - While the Player, after a very difficult boss fight, one that carries from the virtual realm to possibly other parts (Non-Harmful file manipulation), does indeed strike down Nando, a sick twist will arise from it
   
  - In Insane Mode, Secret Obtainables may be possible to unlock this Secret ending, which would play after the games credit sequence
    - This Secret ending reveals that an unknown entity had leaked into Nando's Realm, and had:
	  - Revived an Older Rendition of Nando to fight the player at the end of the tutorial
	  - Reprogrammed this Nando before deployment to seek out both the realm and power stars
	  - As the game also progressed, this unknown entity was also using other power stars towards making MANY other nandos
	- As the Nandos are shot through the Dimensional Rift/Tear that brought the unknown entity to his realm, a brief shot of him is relatively similar to <https://i.imgur.com/Vhp7uq0.jpg>
	  - The Image and entity brief revelation is a small tease to the boss originally intended for "Terriful takes back the World"
	  - It also means that not only are the Tough Universe Games combines, but the Dimensional Tear resulted from a cascade event in TTBTW
	  - *The Dimensional Tear also was reprogrammed by this mysterious entity to go to any dimension within the multiverse, intended to both spread his and Nando's now jointed personality, but also a possible conflict for Terriful and other games should the door be opened again*
   
* While Some Ideas here are very JSAB Inspired, work will be done to ensure the play styles are not exactly alike from each other *

Abilities:
 - The New Player, at the start and through a bit of the game, will be quite weak, only being able to dodge (But will later acheive a shield as well as learning to operate mechanisms and techniques to fight back + The Sword)
 - Nando's Power will exceed that of TC Live IV and ReCoded, both with new effects for the users screen such as Inversion, Glitch-Fest, as well as being able to call upon both new and old enemies
   - These Enemies include:
     - The Nothing which chases the players in specially built levels, and is able to increase/decrease speed, and invert the side of the screen it approaches from
	 - Crabs that eventually gain abilities to move fast and dash toward the player
	 - Oruga's able to change their own speed at any time and change their spin direction
	 - A new teleporting entity, able to appear near the player and is very quick moving
	 - The Master of Trolls and possibly a giant mech, both while able to perform quick attacks to the player, can be clumsy and attack themselves if attacked properly
   - Nando's New Abilities:
     - Ability to spawn powerups that can slow down the game by a certain degree, and in insane mode, a fake powerup that when collected, returns you to the level map instantly
	 - Can invert the map itself either reversing it's direction, or rotating vertically, offering a new challenge and degree to the player
	 - Unlike other games, depending on Nando's current power level, his punches can either kill the player or force the player backwards in the map while the sheer force of his punch randomizes the events
	 - Nando is able to talk to the player through both in-game dialog boxes as well as Native OS' Dialogs, producing the effect that Nando has much more control over the game then you do.
	 - Nando is also able to distort audio or textures to mess with you, if he chooses (Also via Random Chance).
	 - Nando is also able to spawn each of the different traps he's used throughout his universe, including spikes, and can also now emit fire towards the player (only blocked by shield)
	 - At any time also, Online Mode and Story Mode (if set in settings) can also collide, so you never know if the Nando your facing is "just programmed" or inputted by someone else.
	   - This is related to the twist ending (Revealed by secrets possibly) that during the weakened fight in the tutorial, Nando had cloned his construct and personality into hundreds of other Nando's, and showcases many of them being sent of and hurtled towards other universes/realms
	     - Aka you freed that realm, but that was only ONE construct, and unknowingly, many more were released elsewhere upon his "death"
	 - Should he choose, Nando can become bigger and smaller as well, manipulating his hitbox to make him a harder hit but also making him weaker/stronger
   - Player/Nando Powerups:
     - [Player/Nando] Health Powerup available in 1,2,or 3 sizes that restores health points to both sides
	 - [Player/Nando] Power Stars: While Nando is able to absorb these fully, The Player can only absorb so much of it, only giving the player temporary extended abilities
	 - [Player] Fake Powerups: Made to mimic other powerups, with inverse effects (Shine is inversed) as well as a duller color palette, 
	 that will either negate your other effects or cause an in-game or irl negativity to occur
   - Mystery Entity:
     - While the player will never directly see or interact with this enemy, IN INSANE MODE, the mysterious enemy can sometimes appear far into thebackground
	   - This gives the illusion that it is watching the player, and perhaps learning from the player, preparing for the secret ending
	   
Full Powerups List:
Heart Powerup[-x3 to x1 to x3]: Increases/Decreases Hearts of either you or your foes, depends who picks them up 
Coin Magnet[-x3 to x1 to x3]: Increases/Decreases the Radius at which Coins go to you or repel from you, or are eaten by the enemies, depending on who pickes it up
Powerup Magnet[-x3 to x1 to x3]: Increases/Decreases the Radius at which powerups go to or repel from you, or are eaten by enemies, depending on who picks it up
Inverse Powerup: Can either Inverse your Current Powerup (In the case of a Gun, Magnet Strength, etc), or change all powerups currently on screen to inverse powerups
Glitch-Fest Powerup: Can cause Mild Glitch Effects, can cause Events to randomize themselves, or in Insane Mode, boot you out of the level
Triple Shot Gun: Allows you to either shoot in 3 seperate directions, or in a closer radius, depending on powerup variant
Wave Gun: Sends out an Energy Wave capable of pushing back enemies depending on Gun Strength, and also can become penetrating, if strong enough
Bullet Upgrade Powerup[-x3 to x1 to x3]: Upgrades for current weapon that can allow better bullet penetration, better fire rate, and an overall stronger/weaker bullet
Shockwave Powerup: Causes a Massive Energy Shockwave, destroying weaker enemies and temporarily disabling some AI components from the after-effects
Beast Egg: Used in Old-Gen, Spawns a Beast that breathes fire

Full Enemy/Obstacle List:
- Mosquitos
  - Annoyin' Little Buggers, both in Real Life and in this game
  - Seriously, so damn many of them?!
- Crabs
  - In Tutorial: Move from Left to Right statically
  - After Takeover: Able to dash and generally head towards the player
- Oruga Big Boss
  - In Tutorial: Move in spiral shapes, normally in only one direction
  - After Takeover: Can go into an Angry Mode, not allowing hits, and can also change their direction to clockwise or counter clockwise
- Bomb Cat
  - In Tutorial: Only moves in straight line, if player touches enemy, enemy explodes in a random radius
  - After takeover: Aimes towards the player, though still explodes in a random radius (But can explode when a bit further from the player)
- Pincho
  - Bops towards the other side of the screen, enemy deals extra damage due to it's spike top
  - Spike top can be shot off before the actual enemy* (Conceptual)
- Waller
  - Uses a Shield that either must be shot down before defeating him, or can be defeated by a HeadShot
  - In Insane Mode, the Waller will move/extend his shield to cover his head and feet, thereby requiring the shield to be destroyed first
  - Can charge a jump, but temporarily looses shield during so, if player his hit by the shockwave, they get stunned. If Hit by the stomp, they'll lose more health then normal
- Asteroid
  - Cannot be defeated with Base Weapons, only get pushed away. Less stronger weapons do less to push it away
  - If Shield is used, along with most stronger enemies, player will get pushed backwords
  - Higher Upgraded Weapons can penetrate, split up the asteroid, then destroy the chunks
- Spinning Fire
  - Some Blocks in the game will spit out fire that can both burn the player and give a persistent effect to the player that lasts a second or two after leaving the fire
- Mystery Blocks
  - These blocks contain either Treasurs such as a powerup or coin, or a sneaky enemy hidding within, such as a crab
- Large Blocks
  - These blocks can only be destroyed by a higher level wave gun, in which the skockwave will be enough to disturb these blocks to destroy them
- Small Blocks // Cracked Block
  - Upgraded guns can easily destroy these after firing for a bit, in which they turned into cracked variants, then break apart
- Space Invaders
  - Spawn in Clusters that move in a retro fashion that stay with the player until defeated or screen moves too quick for them
- Taladril
  - *From Old Gen, need input on function*
- Bony Baddie
  - *From Old Gen -- Live Gen, needs further input*
- Careta
  - *From Old Gen, needs further input*
- Coin Eater (Old Gen)
  - Aims for coins in an arching stride to steal your coins
  - if his mouth touches coins, they will disappear as being eaten
  
Timeline:
  - Tough Coded : RPG Era - Period of Experimentation, with it's concepts coming back into fruitition in Remixed
  - Tough Coded X - First Initial Implementation of LIVE Gameplay, also included sub stages and created in Clickteam Fusion
  - Tough Coded Live IV - Ported to Unity, major graphical overhaul, remvoed sub stages due to copyright Claims

  - Tough Remixed - This Game, also made in Unity as new standard
  - Tough ReCoded - While Releasing before this, is better of being known as an AU for timeline consistency
  
Removed Features:
- Mario Fire: Is being redesigned to avoid any posibility of Copyright Claims
- George Clooney: Noone really ever got permission to put him in, and while I'm sure he'd like it, to avoid trouble, I unfortunatly can't keep him in for now
- Coins: Will utilize the sound effects from Tough ReCoded but retain their design from Tough Coded (In live IV Design Mode)
- Store: While I'm not certain if this should return, this is being kept here until I can make a good choice for it (Might have some RPG Elements added into it)
	 
	 
** This Document is a Conceptual Document, and is Subject to Change frequently for additions or removals **