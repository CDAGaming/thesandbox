*My time with helping out Max Level Studios may be over,*
*and while I wish them well, my Story has only just begun :)*

## Introducing PROJECT SANDBOX!

Project Sandbox is a collection of Mini-Games, experiments, and other odd stuff performed by CDA.

These experiments are a mix of deprecated or unused features from prior projects, as well as new stuff I pick up and experiment with along the way.

All Code in this Project was created/dominantly owned by CDA or other open sourced parties, and most assets have been approved before usage (Except Nintendos)

## Game List:
- Tough Remixed
  - Spin Off from the Tough Coded Universe involving the prospect of not being able to fight back against Nando, only reliant on dodging his obstacles until late game.
  - Will involve a Story Mode involving Nando, possibly speaking to the original Cody, alongside reviving old conflicts with new and old enemies alike
  - Will Feature Revivals from TC's RPG Days, conflicts with The Nothing, and other characters
  - Theoretical finale and Ultimate Ending to the Tough Coded Games and as fallback for if ReCoded fades out

- Mario Remake (Due to licensing, this will only be available as a privated build, with this module included)
  - Remake of at least the first level of SMB using Nintendos Original Assets with a mode to also switch assests to the other 2D sprites
  - (3D Sprites can be made possible but involves work I wont have time for)
  - Possibly also adding a Battle Royale Mode much like DMCA Royale

## Utility/Testing Modules:
 - Localization Editor
   - Port of the Translation Engine CraftPresence uses in Java to C#, giving any person who uses it a way to localize whatever they choose for any language with Unicode support
 - Automated Update Checker
   - Ability to supply an Update Json, which is checked upon launch with support of redirecting you to a download link)
 - Config Editor
   - Design Configs for Modules into Json that will be parsed on Module Load
 - Song Viewer
   - Allows for downloading most types of audio files, and streaming it to an Internal AudioClip
  
## How Much?
Following my other projects, I do this type of stuff for the community.
If I ever do need the money, I can open up donations, though as is, All my project both are Free to Use, Modify, Redistribute as well as are Open sourced to support this

If you ever do wish to support me, I'll be opening up something for donations in 2019 Q4 approx.

## Alpha/Beta Schedule
Ironically, in most of my work, I don't typically like to publish something unfinished/buggy 
(Even if it's to a small pool of people, I'm still very much on edge when doing those)
But, should any Builds be stable enough for a small test, I'll be pushing them out to testers as they become ready

## Release Projections // Availability
Most likely for the time being, this will be on my own Discord or the Discord Store 
(As noone wants to pay *too* much to put up a game but not too little either)

## NetCode + Networking Resources
Networking in this App, while some of it will depend on what the OPs choose, most of the App will depend on the Photon Engine (PUN 2) as well as connecting with Doucl Services.
Do note, that neither service will store any personal data from you.